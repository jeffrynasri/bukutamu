<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->islogin();
	}

	protected function islogin(){
		if(!$this->session->has_userdata(SESSION_LOGIN_USERNAME)){
			redirect('authen');
		}
	}

	//CONVERT Tanggal Indo
  protected function print_tanggal_formatindo($time){
    $number_month = date('m',strtotime($time));
    $number_week = date('N',strtotime($time));
    $day=hari_indo($number_week,'l');
    $tanggal =date('d',strtotime($time));
    $month = bulan_indo($number_month,'m');
    $year = date('Y',strtotime($time));
    $fullday = $tanggal . " " . $month . " " . $year . "(" . $day . ")";
    return $fullday;
  }
}
