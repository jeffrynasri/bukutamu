<?php

 
class Loguser_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    

    function get_loguser_allparams($params)
    {
        return $this->db->get_where('loguser',$params)->row_array();
    }
    /*
     * Get loguser by loguser
     */
    function get_loguser($id)
    {
        return $this->db->get_where('loguser',array('logid'=>$id))->row_array();
    }
    
    /*
     * Get all loguser count
     */
    function get_all_loguser_count()
    {
        $this->db->from('loguser');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all loguser
     */
    function get_all_loguser($params = array())
    {
        $this->db->order_by('logid', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('loguser')->result_array();
    }

   
    /*
     * function to add new loguser
     */
    function add_loguser($params)
    {
        $this->db->insert('loguser',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update loguser
     */
    function update_loguser($loguser,$params)
    {
        $this->db->where('loguser',$loguser);
        return $this->db->update('loguser',$params);
    }
    
    /*
     * function to delete loguser
     */
    function delete_loguser($loguser)
    {
        return $this->db->delete('loguser',array('loguser'=>$loguser));
    }
}
