<?php

class Setting_model extends CI_Model
{
    // id
    // password_dewa	
    // pengumuman
    // is_camera_active
    // is_popup_active

    var $table = 'setting';
    var $column_order = array('setting.limit_vaksin'); //set column field database for datatable orderable
    var $column_search = array('setting.limit_vaksin');//set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('setting.limit_vaksin' => 'asc'); // default order
  
    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array())
    {
      $this->db->where($params);
      $this->db->select('setting.*');
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
  
    function get_datatables($params=array())
    {
      $this->_get_datatables_query( $params?$params:array() );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array())
    {
      $this->_get_datatables_query($params);
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array())
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get setting by id
     */
    function get_setting($id)
    {
        return $this->db->get_where('setting',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all setting
     */
    function get_all_setting($params=array())
    {
        $this->db->where($params);
        return $this->db->get('setting')->result_array();
    }
        
    /*
     * function to add new setting
     */
    function add_setting($params)
    {
        $this->db->insert('setting',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update setting
     */
    function update_setting($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('setting',$params);
    }
    
    /*
     * function to delete setting
     */
    function delete_setting($id)
    {
        return $this->db->delete('setting',array('id'=>$id));
    }
}
