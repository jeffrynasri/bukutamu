<?php

class Kunjungan_model extends CI_Model
{
    // id
    // tanggal
     // pulang
    // nama
    // no_hp
    // keterangan
    // instansi
    // id_tujuan
    // id_layanan	
    //email
    var $table = 'kunjungan';
    var $column_order = array('kunjungan.id','kunjungan.tanggal','kunjungan.pulang','kunjungan.nama','kunjungan.no_hp','kunjungan.keterangan','kunjungan.instansi','kunjungan.id_tujuan','kunjungan.id_layanan','kunjungan.email'); 
    var $column_search = array('kunjungan.id','kunjungan.tanggal','kunjungan.pulang','kunjungan.nama','kunjungan.no_hp','kunjungan.keterangan','kunjungan.instansi','kunjungan.id_tujuan','kunjungan.id_layanan','kunjungan.email');
    var $order = array('kunjungan.nama' => 'asc'); // default order

    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='kunjungan.*',$order_by="nama")
    {
      $this->db->where($params);
      $this->db->order_by($order_by, 'asc');
      $this->db->select($select.',tujuan.nama as tujuan_nama,layanan.nama as layanan_nama');
      $this->db->join('tujuan','kunjungan.id_tujuan=tujuan.id');
      $this->db->join('layanan','kunjungan.id_layanan=layanan.id');
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
      
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='kunjungan.*',$order_by="nama")
    {
      $this->_get_datatables_query( $params?$params:array(),$select?$select:'kunjungan.*',$order_by?$order_by:'nama' );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='kunjungan.*',$order_by="nama")
    {
      $this->_get_datatables_query($params?$params:array(),$select?$select:'kunjungan.*',$order_by?$order_by:'nama');
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='kunjungan.*',$order_by="nama")
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }

    function search_name($title){
        $this->db->like('nama', $title , 'both');
        $this->db->order_by('nama', 'ASC');
        $this->db->limit(10);
        return $this->db->get('kunjungan')->result();
    }
    function search_nohp($title){
        $this->db->like('no_hp', $title , 'both');
        $this->db->order_by('no_hp', 'ASC');
        $this->db->limit(10);
        return $this->db->get('kunjungan')->result();
    }
    function search_instansi($title){
        $this->db->like('instansi', $title , 'both');
        $this->db->order_by('instansi', 'ASC');
        $this->db->limit(10);
        return $this->db->get('kunjungan')->result();
    }
    function search_keterangan($title){
        $this->db->like('keterangan', $title , 'both');
        $this->db->order_by('keterangan', 'ASC');
        $this->db->limit(10);
        return $this->db->get('kunjungan')->result();
    }

    function search_email($title){
        $this->db->like('email', $title , 'both');
        $this->db->order_by('email', 'ASC');
        $this->db->limit(10);
        return $this->db->get('kunjungan')->result();
    }

    
    function get_kunjungan_filterrby_monthyearidtujuan_groupby_idjuan($tgl,$bulan,$tahun,$id_tujuan){
      $query = "SELECT id_tujuan,COUNT(nama) as jumlah FROM kunjungan WHERE year(tanggal)=".$tahun." and day(tanggal)=".$tgl." and month(tanggal)=".$bulan." and id_tujuan=".$id_tujuan;
      return $this->db->query($query)->result_array();
    }
    function get_report_kunjungan_perdate($tgl,$bulan,$tahun){
        $query = "SELECT tanggal,COUNT(a.nama) as jumlah, b.nama as namak FROM kunjungan a INNER JOIN tujuan b ON a.id_tujuan = b.id WHERE year(tanggal)=".$tahun." and day(tanggal)=".$tgl." and month(tanggal)=".$bulan." GROUP BY id_tujuan DESC";
        $data = $this->db->query($query);
        return $data->result_array();
    }
    function get_report_kunjungan_permonth_bynama($bulan,$tahun,$nama){
        $query = "select COUNT(*) as jumlah, lengkap.nama FROM (SELECT tujuan.nama,kunjungan.tanggal FROM `kunjungan` JOIN tujuan ON kunjungan.id_tujuan = tujuan.id) as lengkap WHERE year(tanggal)=".$tahun. " AND month(tanggal)=".$bulan." AND lengkap.nama='". $nama."'"; 

        $data = $this->db->query($query);
        return $data->result_array();
    }
    function get_kunjungan_not_pulang(){
      $query = "SELECT * FROM `kunjungan` WHERE DATE(`tanggal`) = CURDATE() and `pulang` is null"; 

      $data = $this->db->query($query);
      return $data->result_array();
    }
    /*
     * Get kunjungan by id
     */
    function get_kunjungan($id)
    {
        return $this->db->get_where('kunjungan',array('kunjungan.id'=>$id))->row_array();
    }
  
    
   /*
     * Get all kunjungan
     */
    function get_all_kunjungan($params=array())
    {
        $this->db->order_by('nama', 'asc');
        $this->db->where($params);
        return $this->db->get('kunjungan')->result_array();
    }
        
    /*
     * function to add new kunjungan
     */
    function add_kunjungan($params)
    {
        return $this->db->insert('kunjungan',$params);
    }
    
    /*
     * function to update kunjungan
     */
    function update_kunjungan($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('kunjungan',$params);
    }
    
    /*
     * function to delete kunjungan
     */
    function delete_kunjungan($id)
    {
        return $this->db->delete('kunjungan',array('id'=>$id));
    }
}
