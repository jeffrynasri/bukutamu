<?php

class Layanan_model extends CI_Model
{
    //id	
    //nama

    var $table = 'layanan';
    var $column_order = array('layanan.nama'); 
    var $column_search = array('layanan.nama');
    var $order = array('layanan.nama' => 'asc'); // default order


    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='layanan.*',$order_by="nama")
    {
      $this->db->where($params);
      $this->db->order_by($order_by, 'asc');
      $this->db->select($select);
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
      
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='layanan.*',$order_by="nama")
    {
      $this->_get_datatables_query( $params?$params:array(),$select?$select:'layanan.*',$order_by?$order_by:'nama' );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='layanan.*',$order_by="nama")
    {
      $this->_get_datatables_query($params?$params:array(),$select?$select:'layanan.*',$order_by?$order_by:'nama');
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='layanan.*',$order_by="nama")
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get layanan by id
     */
    function get_layanan($id)
    {
        return $this->db->get_where('layanan',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all layanan
     */
    function get_all_layanan($params=array())
    {
        $this->db->order_by('nama', 'asc');
        $this->db->where($params);
        return $this->db->get('layanan')->result_array();
    }
        
    /*
     * function to add new layanan
     */
    function add_layanan($params)
    {
        $this->db->insert('layanan',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update layanan
     */
    function update_layanan($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('layanan',$params);
    }
    
    /*
     * function to delete layanan
     */
    function delete_layanan($id)
    {
        return $this->db->delete('layanan',array('id'=>$id));
    }
}
