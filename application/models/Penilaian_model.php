<?php

 
class Penilaian_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_detail_penilaian_groupbynilai($nilai,$where=array()){
        $where['nilai']=$nilai;
        $this->db->select('nilai, count(*) as jumlah,aspek_skm.nama');
        $this->db->join('aspek_skm', 'penilaian.id_aspek_skm = aspek_skm.id');
        $this->db->join('skm','penilaian.id_skm=skm.id');
        $this->db->group_by("aspek_skm.nama");
        //$this->db->order_by("jumlah", "desc");
        return $this->db->get_where('penilaian',$where)->result_array();
    }

    function get_penilaian_groupbyilai($where=array()){

        $this->db->select('nilai, count(*) as jumlah');
        $this->db->join('skm','penilaian.id_skm=skm.id');
        $this->db->group_by("nilai");
        $this->db->where($where);
        //$this->db->order_by("jumlah", "desc");
        return $this->db->get("penilaian")->result_array();

    }

    function get_report_survey_permonth($bulan,$tahun){
        // $this->load->model('Penilaian_model', 'penilaian');
        //   $this->load->model('Aspek_skm_model', 'penilaian');
        //  $result = $this->blog->method_on_blog_model();

        $query = "select COUNT(*) as jumlah, nilai FROM (SELECT * FROM `penilaian` JOIN skm ON penilaian.id_skm = skm.id) as lengkap WHERE year(tanggal)=". $tahun ." AND month(tanggal)=". $bulan ." GROUP BY nilai ";

        $data = $this->db->query($query);
        return $data->result_array();
    }

    function get_report_survey_permonthbynilai($bulan,$tahun,$nilai,$where=array()){
        $where['year(tanggal)']=$tahun;
        $where['month(tanggal)']=$bulan;
        $where['nilai']=$nilai;

        $this->db->select();
        $this->db->join('skm','penilaian.id_skm=skm.id');
        $this->db->where($where);
        return $this->db->get('penilaian')->result_array();

        // $query = "select COUNT(*) as jumlah, nilai FROM (SELECT * FROM `penilaian` JOIN skm ON penilaian.id_skm = skm.id) as lengkap WHERE year(tanggal)=". $tahun ." AND month(tanggal)=". $bulan ." AND nilai=". $nilai;

        // $data = $this->db->query($query);
        // return $data->result_array();
    }

    function get_penilaian_byskm($id_skm)
    {
        //return $this->db->get_where('penilaian',array('id_skm'=>$id_skm))->result_array();
         $this->db->select("*");
        $this->db->join('aspek_skm', 'penilaian.id_aspek_skm = aspek_skm.id');
        $this->db->where("id_skm",$id_skm);
        return $this->db->get("penilaian")->result_array();
    }


    /*
     * Get penilaian by id_aspek_skm
     */
    function get_penilaian($id_aspek_skm)
    {
        return $this->db->get_where('penilaian',array('id_aspek_skm'=>$id_aspek_skm))->row_array();
    }
        
    /*
     * Get all penilaian
     */
    function get_all_penilaian()
    {
        $this->db->order_by('id_aspek_skm', 'desc');
        return $this->db->get('penilaian')->result_array();
    }
        
    /*
     * function to add new penilaian
     */
    function add_penilaian($params)
    {
        $this->db->insert('penilaian',$params);
        return 1;
    }
    
    /*
     * function to update penilaian
     */
    function update_penilaian($id_aspek_skm,$params)
    {
        $this->db->where('id_aspek_skm',$id_aspek_skm);
        return $this->db->update('penilaian',$params);
    }
    
    /*
     * function to delete penilaian
     */
    function delete_penilaian($id_skm)
    {
        return $this->db->delete('penilaian',array('id_skm'=>$id_skm));
    }
}
