<?php

class Tujuan_model extends CI_Model
{
    //id	
    //nama

    var $table = 'tujuan';
    var $column_order = array('tujuan.nama'); 
    var $column_search = array('tujuan.nama');
    var $order = array('tujuan.nama' => 'asc'); // default order


    function __construct()
    {
      parent::__construct();
    }
    //UNTUK DATATABEL
    private function _get_datatables_query($params=array(),$select='tujuan.*',$order_by="nama")
    {
      $this->db->where($params);
      $this->db->order_by($order_by, 'asc');
      $this->db->select($select);
      $this->db->from($this->table);
      $i = 0;
  
      foreach ($this->column_search as $item) // looping awal
      {
        if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
        {
  
          if($i===0) // looping awal
          {
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }
  
          if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
        }
        $i++;
      }
      
  
      if(isset($_POST['order']))
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
    function get_datatables($params=array(),$select='tujuan.*',$order_by="nama")
    {
      $this->_get_datatables_query( $params?$params:array(),$select?$select:'tujuan.*',$order_by?$order_by:'nama' );
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }
  
    function count_filtered($params=array(),$select='tujuan.*',$order_by="nama")
    {
      $this->_get_datatables_query($params?$params:array(),$select?$select:'tujuan.*',$order_by?$order_by:'nama');
      $query = $this->db->get();
      return $query->num_rows();
    }
    public function count_all($params=array(),$select='tujuan.*',$order_by="nama")
    {
      $this->db->from($this->table);
      $this->db->where($params);
      return $this->db->count_all_results();
    }
    
    /*
     * Get tujuan by id
     */
    function get_tujuan($id)
    {
        return $this->db->get_where('tujuan',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all tujuan
     */
    function get_all_tujuan($params=array())
    {
        $this->db->order_by('nama', 'asc');
        $this->db->where($params);
        return $this->db->get('tujuan')->result_array();
    }
        
    /*
     * function to add new tujuan
     */
    function add_tujuan($params)
    {
        $this->db->insert('tujuan',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update tujuan
     */
    function update_tujuan($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('tujuan',$params);
    }
    
    /*
     * function to delete tujuan
     */
    function delete_tujuan($id)
    {
        return $this->db->delete('tujuan',array('id'=>$id));
    }
}
