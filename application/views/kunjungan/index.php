<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Tamu</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="box-body">
                        <div class='row'>
                            <div class='col-md-6'>
                                <span>Dari</span>
                                <input type='date' name='waktu_dari' id='waktu_dari' value="<?php echo date('Y-m-d'); ?>" />
                            </div>
                            <div class='col-md-12'>
                                <span>Sampai</span>
                                <input type='date' name='waktu_sampai' id='waktu_sampai' value='<?php echo date('Y-m-d'); ?>' />
                            </div>
                            <div class='col-md-12'>
                                <span>Tempat</span>
                                <select name='id_tujuan' id='id_tujuan'>
                                    <option value='-1' selected>Semua</option>
                                    <?php foreach($all_tujuan as $tujuan){ ?>
                                        <option value="<?php echo $tujuan['id']; ?>"><?php echo $tujuan['nama']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class='col-md-12'>
                                <span>Layanan</span>
                                <select name='id_layanan' id='id_layanan'>
                                    <option value='-1' selected>Semua</option>
                                    <?php foreach($all_layanan as $layanan){ ?>
                                        <option value='<?php echo $layanan['id']; ?>'><?php echo $layanan['nama']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class='col-md-12'>
                                <button id='filter' class='btn btn-primary' >Pilih</button>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-lg-3 col-6'>
                                <div class='small-box bg-info btn-info'>
                                    <div class='inner'>
                                        <h3 id='total_tamu'>-</h3>
                                        <p>Tamu</p>
                                    </div>
                                    <div class='icon'>
                                        <i class='fa fa-group'></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" >
                        <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                           <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                    <th>No Hp</th>
                                    <th>Instansi</th>
                                    <th>Tujuan</th>
                                    <th>Layanan</th>
                                    <th>Email</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#custom_datatable').DataTable({ 
            dom: 'lBfrtip',
            buttons:[
                'copy','csv','excel','print',{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize:'A3',
                }
            ],
            iDisplayLength: 25,
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                "url": "<?php echo site_url('kunjungan/get_data_kunjungan_json')?>",
                'data':function(d){
                    //console.log($('#id_tujuan').val())
                    d.waktu_dari = $('#waktu_dari').val(),
                    d.waktu_sampai = $('#waktu_sampai').val(),
                    d.id_tujuan = $('#id_tujuan option:selected').val(),
                    d.id_layanan = $('#id_layanan option:selected').val()                    
                },
                // 'data':{
                //     "waktu_dari": $('#waktu_dari').val(),
                //     "waktu_sampai": $('#waktu_sampai').val(),
                //     "id_tujuan": $('#id_tujuan option:selected').val(),
                //     "id_layanan": $('#id_layanan option:selected').val()                    
                // },
                "type": "POST",
                'dataSrc': function(json){
                    console.log("hua");
                    console.log(json.recordsTotal)
                    $('#total_tamu').text(parseInt(json.recordsTotal))
                    return json.data
                }
                
            },
            
            "columnDefs": [
                { 
                    "targets": [ 7,8], 
                    "orderable": false, 
                },
                {'targets':0, 'width': '10%'},
                {'targets':1, 'width': '20%'},
                {'targets':2, 'width': '10%'},
                {'targets':3, 'width': '10%'},
                {'targets':4, 'width': '10%'},
                {'targets':5, 'width': '10%'},
                {'targets':6, 'width': '10%'},
                {'targets':7, 'width': '10%'},                
                {'targets':8, 'width': '10%'},                
            ],
 
        });
        $('#filter').click(function(){
            console.log("hura");
            $('#custom_datatable').DataTable().ajax.reload();
        })
        //$('#custom_datatable').DataTable().ajax.reload();
    }); 
 
</script>