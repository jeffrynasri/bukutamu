<?php
	if($this->session->flashdata('pesan')){
		echo "<script> M.toast({html: '".$this->session->flashdata('pesan')."'}) </script>";
	};
?>

 <!-- Modal Structure -->

<div class="container">
    <div class='row'>
        <div class="col s12">
             <a href="<?php echo site_url('authen/index'); ?>" style="margin-left: 8px;" class="right btn waves-effect waves-light">Login<i class="material-icons right">login</i></a> 
            <!--<a href="https://desagadingwatu.gresikkab.go.id/layanan-mandiri/masuk" target="_blank" style="margin-left: 8px;" class="right btn waves-effect waves-light">Laynan Mandiri<i class="material-icons right">touch_app</i></a>-->
            <!--<a href="<?php echo site_url('beranda/skm'); ?>" style="margin-left: 8px;" class="right btn waves-effect waves-light">SKM<i class="material-icons right">comment</i></a>-->
            <a href="https://sukma.jatimprov.go.id/fe/survey?idUser=1186" style="margin-left: 8px;" class="right btn waves-effect waves-light">SKM<i class="material-icons right">comment</i></a>
            <a href="<?php echo site_url('beranda/pulang'); ?>" style="margin-left: 8px;" class="right btn waves-effect waves-light">Pulang<i class="material-icons right">logout</i></a>
            <a href="<?php echo site_url('beranda/index'); ?>" class="right btn waves-effect waves-light">Buku Tamu<i class="material-icons right">book</i></a>
        </div>
    </div>
    <div class="row">
        <?php //$this->load->view('home/papan_informasi'); ?>
        <div class="col s12">
            <h3 class="center text-center">BUKU TAMU KAB GRESIK</h3>
        </div>

        <form class="col s12" method="post" action="<?php echo site_url('beranda/index'); ?>">
            <div class="row">
                <label style="margin-left:8px;">Tempat</label>
                <div class="input-field col s12">
                    <select name="id_tujuan" id="id_tujuan">
                        <?php foreach($all_tujuan as $tujuan){ ?>
                            <option value='<?php echo $tujuan['id']; ?>'><?php echo $tujuan['nama']; ?></option>
                        <?php } ?>
                    </select>
                    <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('id_tujuan');?></span>
                </div>
            </div>
            <div class="row">
                <label style="margin-left:8px;">Layanan</label>
                <div class="input-field col s12">
                    <select name="id_layanan" id="id_layanan">
                        <?php foreach($all_layanan as $layanan){ ?>
                            <option value='<?php echo $layanan['id']; ?>'><?php echo $layanan['nama']; ?></option>
                        <?php } ?>
                    </select>
                    <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('id_layanan');?></span>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                <input  value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : ""); ?>" name="nama" id="nama" type="text" class="validate">
                <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('nama');?></span>
                <label for="nama">Nama (Wajib diisi)</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                <input  value="<?php echo ($this->input->post('no_hp') ? $this->input->post('no_hp') : ""); ?>" name="no_hp" id="no_hp" type="number" class="validate">
                <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('no_hp');?></span>
                <label for="no_hp">No HP (Wajib diisi)</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                <input  value="<?php echo ($this->input->post('instansi') ? $this->input->post('instansi') : ""); ?>" name="instansi" id="instansi" type="text" class="validate">
                <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('instansi');?></span>
                <label for="instansi">Asal Instansi (Wajib diisi)</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                <input  value="<?php echo ($this->input->post('keterangan') ? $this->input->post('keterangan') : ""); ?>" name="keterangan" id="keterangan" type="text" class="validate">
                <span class="helper-text" data-error="wrong" data-success="right"><?php echo form_error('keterangan');?></span>
                <label for="keterangan">Keterangan</label>
                </div>
            </div>
            <button id='kirim' class="right btn waves-effect waves-light" type="submit" name="action" onclick='return confirm(" Apakah Data yang Dikirim sudah Benar ?")'>Kirim
                <i class="material-icons right">send</i>
            </button>
        </form>
    </div>
</div>

  <script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    });
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
    // Or with jQuery
    function get_json_jadwal(){
        //Validate fields if required using jQuery        
        var postForm = { //Fetch form data
            'id_id_tujuan'     : $('#id_tujuan option:selected').val()
        };
        
        $.ajax({ //Process the form using $.ajax()
            type      : 'POST', //Method type
            url       : '<?php echo site_url("beranda/finding_slot_vaksin_json"); ?>', //Your form processing file URL
            data      : postForm, //Forms name
            dataType  : 'json',
            success   : function(data) {
                        //console.log(data)    
                        if(data['isthereopenslot']=='1'){
                            $('#kirim').attr('disabled',true);
                            data['list_tanggalvaksin_open'].forEach(function(item,index){
                                var tanggal = new Date(item['tanggal']);
                                var isi_option=('0' + tanggal.getDate()).slice(-2) + '-' + ('0' +(tanggal.getMonth()+1)).slice(-2) + '-' + tanggal.getFullYear()+ " tersedia " + item['sisa'] + " vaksin"
                                $("#waktu").append($("<option></option>").attr("value",item['tanggal']).text(isi_option));
                            })
                            $('#waktu_error').text(data['string_tanggalvaksin_penuh']);
                            $('#kirim').removeAttr('disabled');
                        } else{
                            $('#waktu_error').text(data['string_tanggalvaksin_penuh']);
                            $('#kirim').attr('disabled',true);
                            M.toast({html: 'Kuota Vaksin Penuh'})
                        }
                        $('#waktu').formSelect();
                        $('#tulisan_loading').hide();
                       
            }
        });
     //   event.preventDefault(); //Prevent the default submit
    }
    function init_jadwal(){
        $('#tulisan_loading').show();         
        $("#waktu").empty().html(' '); 
        $('#waktu').formSelect();
        get_json_jadwal()
    }
    $(document).ready(function(){
        <?php if($setting['is_popup_active']==1){ ?>
            $('.modal').modal();
            //now you can open modal from code
            $('#modal1').modal('open');

            //or by click on trigger
            $('.trigger-modal').modal();
        <?php } ?>
        init_jadwal()
    });

    $('#id_tujuan').on('change', function() {
        //console.log( this.value );
        init_jadwal();
    });
  </script>