<style>
	.marginbottom {
	  margin-bottom: 6px;
	}
</style>
<!-- DataTable 1.10.19-->
<link rel="stylesheet" href="<?php echo base_url();?>resources/css/material.min.css" defer>
<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery.dataTables.min.css" defer>
<link rel="stylesheet" href="<?php echo base_url();?>resources/css/responsive.dataTables.css" defer>
<link rel="stylesheet" href="<?php echo base_url();?>resources/css/buttons.dataTables.min.css" defer>
<script src="<?php echo base_url();?>resources/js/jquery.dataTables.min.js" defer></script>
<script src="<?php echo base_url();?>resources/js/dataTables.responsive.js" type="text/javascript" defer></script>
<script src="<?php echo base_url();?>resources/js/dataTables.buttons.min.js" defer></script>
<script src="<?php echo base_url();?>resources/js/buttons.flash.min.js" defer></script>
<script src="<?php echo base_url();?>resources/js/jszip.min.js" defer></script>
<script src="<?php echo base_url();?>resources/js/pdfmake.min.js" defer></script>
<script src="<?php echo base_url();?>resources/js/vfs_fonts.js" defer></script>
<script src="<?php echo base_url();?>resources/js/buttons.html5.min.js" defer></script>
<script src="<?php echo base_url();?>resources/js/buttons.print.min.js" defer></script>


<div class="row">
	<div class="col s12">
		<a href="<?php echo site_url('beranda/index'); ?>" class="left btn waves-effect waves-light">Beranda<i class="material-icons right">home</i></a>
	</div>
	<div class="row container">
		<div class="center-align col s12 ">
			<img class="responsive-img" width="15%" height="15%" src="<?php echo base_url();?>resources/img/logolpse.png"/>
		</div>
		<?php $this->load->view("home/kotak_pencarian"); ?>
	</div>
	<div class="col s12">
		<p>Pencarian Anda : </p>
		<?php echo $pencarian;?>
	</div>
	<div class="col s12" >
		<?php if($permohonan_covid){ ?>
			<table id="datatable" class="display table-hover dt-responsive" width="100%">
				<thead>
					<tr>
						<th>Tanggal Vaksin</th>
						<th>Tempat Vaksin</th>
						<th>NIK</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>Keterangan</th>
					</tr>
				</thead>
				<tbody>
						<tr>
							<td><?php echo date("d/m/Y", strtotime($permohonan_covid['tanggal_permohonan'])); ?></td>
							<td><?php echo $permohonan_covid['tempat_vaksin_nama']; ?></td>
							<td><?php echo $permohonan_covid['nik']; ?></td>
							<td><?php echo $permohonan_covid['nama']; ?></td>
							<td><?php echo $permohonan_covid['alamat']; ?></td>
							<td><?php echo $permohonan_covid['status_vaksin_nama']; ?></td>
						</tr>
				</tbody>
				<tfoot>
				<tfoot>
					<tr class="dataFooter">
						<td>*Membawa <b><?php echo $setting['narasi_persyaratan_administrasi']; ?></b> Saat Hadir</td>
					</tr>
					</tfoot>
				</tfoot>
			</table>
			
		<?php }else{ ?>
			<h3 class='text text-center center'>NIK Tersebut Belum Pernah melakukan Pendaftaran Vaksinisasi</h3>
		<?php }?>
	</div>
</div>

<!-- buttons: [
          { extend: 'pdfHtml5', footer: true }
			], -->

      <!-- buttons: [
            {
              extend: 'pdfHtml5', 
              footer: true,                    
              customize: function (doc) {        
                doc.defaultStyle.font  = 'crackvetica';
              }
            }        
      ]
       -->
<!-- 'copy', 'csv', 'excel', 'pdf', 'print' -->
<script>
  $(function () {
    //GAWE UBAH FONT EXPORT PDF
    // pdfMake.fonts = {
    //         crackvetica: {
    //                 normal: '<?php echo site_url('resources/pont/crackvetica.ttf'); ?>',
    //                 bold: '<?php echo site_url('resources/pont/crackvetica.ttf'); ?>',
    //                 italics: '<?php echo site_url('resources/pont/crackvetica.ttf'); ?>',
    //                 bolditalics: '<?php echo site_url('resources/pont/crackvetica.ttf'); ?>'
    //         }
    // };

    $('#datatable').DataTable({ 
      dom: 'Bfrtip',
      buttons: [
          { extend: 'pdfHtml5', footer: true }
			]
      ,
			"bLengthChange": false,
			"pageLength": 20,
      "language": {
        "lengthMenu": "Tampilkan _MENU_ Data per Halaman",
        "zeroRecords": "Data Tidak Ditemukan.",
        "info": "Halaman Ke- _PAGE_ Dari _PAGES_",
        "infoEmpty": "Halaman Ke- 0 Dari 0",
        "infoFiltered": "(terfilter dari _MAX_ data)",
        "loadingRecords": "Mohon Tunggu...",
        "processing": "Sedang Diproses...",
        "search": "Cari:",
        "bSort": true,
        "pageLength":5,
        "paginate": {
          "previous": "Sebelumnya",
          "next": "Selanjutnya"
        }

      }
    })
  })
</script>