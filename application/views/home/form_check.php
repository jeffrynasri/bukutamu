<style>
	html, body, .my-wrapper {
		height: 100%;
		width: 100%;
	}
</style>
<div class="row valign-wrapper my-wrapper">
	<div class="row container">
		<div class="row">
			<div class="col s12">
				<a href="<?php echo site_url('beranda/index'); ?>" class="left btn waves-effect waves-light">Beranda<i class="material-icons right">home</i></a>
			</div>
			<?php $this->load->view('home/papan_informasi'); ?>
			<div class="center-align col s12 ">
				<!-- <img class="responsive-img" width="15%" height="15%" src="<?php echo base_url();?>resources/img/logolpse.png"/> -->
				<h3 class="center">Unduh Pendaftaran Vaksinisasi Kab Gresik di WEP</h3>
			</div>
		</div>
		<?php $this->load->view("home/kotak_pencarian"); ?>
	</div>
</div>