<?php
	if($this->session->flashdata('pesan')){
		echo "<script> M.toast({html: '".$this->session->flashdata('pesan')."'}) </script>";
	};
?>

 <!-- Modal Structure -->

<div class="container">
    <div class='row'>
        <div class="col s12">
        <a href="<?php echo site_url('authen/index'); ?>" style="margin-left: 8px;" class="right btn waves-effect waves-light">Login<i class="material-icons right">login</i></a> 
            <!--<a href="https://desagadingwatu.gresikkab.go.id/layanan-mandiri/masuk" target="_blank" style="margin-left: 8px;" class="right btn waves-effect waves-light">Laynan Mandiri<i class="material-icons right">touch_app</i></a>-->
            <!--<a href="<?php echo site_url('beranda/skm'); ?>" style="margin-left: 8px;" class="right btn waves-effect waves-light">SKM<i class="material-icons right">comment</i></a>-->
            <a href="https://sukma.jatimprov.go.id/fe/survey?idUser=1186" style="margin-left: 8px;" class="right btn waves-effect waves-light">SKM<i class="material-icons right">comment</i></a>
            <a href="<?php echo site_url('beranda/pulang'); ?>" style="margin-left: 8px;" class="right btn waves-effect waves-light">Pulang<i class="material-icons right">logout</i></a>
            <a href="<?php echo site_url('beranda/index'); ?>" class="right btn waves-effect waves-light">Buku Tamu<i class="material-icons right">book</i></a>
        </div>
    </div>
    <div class="row">
        <?php //$this->load->view('home/papan_informasi'); ?>
        <div class="col s12">
            <h3 class="center text-center">SURVEY KEPUASAN MASYARAKAT</h3>
        </div>

        <form class="col s12" method="post" action="<?php echo site_url('beranda/skm'); ?>">
            <div class="row">
                <label>Tempat</label>
                <div class='input-field col s12'>
                    <select id='id_tujuan' name='id_tujuan'>
                        <?php foreach($all_tujuan as $tujuan){ ?>
                            <option value ='<?php echo $tujuan['id']; ?>' ><?php echo $tujuan['nama']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php foreach($all_aspek_skm as $aspek_skm){ ?>
                <div class="row">
                    <h5><?php echo $aspek_skm['nama']; ?></h5>
                    <p>
                        <label>
                            <input type="radio" name="<?php echo str_replace(' ','-',$aspek_skm['nama']); ?>" value='1'/>
                            <span>Kurang </span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input type="radio" name="<?php echo str_replace(' ','-',$aspek_skm['nama']); ?>" value='2'/>
                            <span>Cukup </span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input type="radio" checked name="<?php echo str_replace(' ','-',$aspek_skm['nama']); ?>" value='3'/>
                            <span>Baik </span>
                        </label>
                    </p>
                    <p><?php echo validation_errors(); ?></p>
                </div>
            <?php } ?>
            <div class="row">
                <div class='input-field col s12'>
                    <input value="<?php echo ($this->input->post('saran')) ? $this->input->post('saran') :''; ?>" name='saran' id="saran" type="text" class="validate" />
                    <label for='saran'>Saran</label>
                </div>
            </div>
            <button id='kirim' class="right btn waves-effect waves-light" type="submit" name="action">Kirim
                <i class="material-icons right">send</i>
            </button>
        </form>
    </div>
</div>

  <script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    });
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
    // Or with jQuery
    
  </script>