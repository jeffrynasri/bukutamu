<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                Dashboard
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info btn-info">
                        <div class="inner">
                            <h3><?php echo count($permohonan_vaksin); ?></h3>
                            <p>Permohonan Vaksin</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-group"></i>
                        </div>
                        <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                    <!-- ./col -->
                    </div>
            </div>
            <div class="box-body">
                <p>*Catatan : Pencarian dengan NIK/NAMA/NO_HP/ALAMAT</p>
                <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                  <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode</th>
                        <th>Nik</th>
                        <th>Tanggal</th>
                        <th>Status Vaksin</th>
                        <th>Nama</th>
                        <th>No Hp</th>
                        <th>Alamat</th>
                    </tr>
                  </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  var table = $('#custom_datatable').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,
    "processing": true,
    "serverSide": true,
    "order": [],

    "ajax": {
      "url": "<?php echo site_url('dashboard/get_data_permohonan_vaksin_json')?>",
      "type": "POST"

    },
    "columnDefs": [
      {
        "targets": [ 7 ],
        "orderable": false,
      },
    ],


  });

});

</script>
