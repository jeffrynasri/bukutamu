<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
        
        <p><?php echo APP_NAME; ?></p>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo site_url('resources/images/user.png');?>" class="user-image" alt="User Image">
                        <span class="hidden-xs">
                            <?php echo $this->session->userdata(SESSION_LOGIN_USERNAME);?>
                            <i class='fa fa-chevron-down'></i>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo site_url('resources/images/user.png');?>" class="img-circle" alt="User Image">
                            <p>
                                <?php echo $this->session->userdata(SESSION_LOGIN_NAMA);?>
                                <br>
                                <small><?php echo $this->session->userdata(SESSION_LOGIN_USERNAME);?></small>
                            </p>
                        </li>
                        <li class="user-body">
                            <div class='text-center'>
                                <p><?php echo $this->session->userdata(SESSION_LOGIN_DESKRIPSI);?></p>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a class='btn btn-danger' href="<?php echo site_url('authen/logout') ?>" class="btn btn-default btn-flat">Keluar</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
