<div class="box-body">
    <div class="col-md-12">
        <div class="box" style="padding: 15px">
            <div class="box-header">
                <h3 class="box-title">Dashboard Buku Tamu</h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                  <?php echo form_open('dashboard') ?>  
                    <div class="row" style="margin-bottom: 6px;">
                      <div class="col-sm-4">    
                        <!-- <label class='control-label' for='id_tujuan'>Tempat</label>    -->
                        <div class="form-group">
                            <select class="form-control mb-1" name="id_tujuan" id='id_tujuan'>
                              <option value="-1">Semua</option>
                              <?php foreach($all_tujuan as $tujuan){ ?>
                                <option value="<?php echo $tujuan['id']; ?>" <?php echo ($this->input->post('id_tujuan') && $this->input->post('id_tujuan')==$tujuan['id'])?'selected':''; ?> ><?php echo $tujuan['nama']; ?></option>
                              <?php } ?>
                            </select>
                        </div>
                      </div> 
                      <div class='col-sm-2'>
                        <button class='btn btn-primary' type="submit">Pilih</button>
                      </div>  
                  <?php echo form_close(); ?>
                </div>
                <div class="col-lg-12 col-xs-12">
                  <div id="graft_line" style="height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<body>
    <script type="text/javascript">
        // Build the chart
    
        Highcharts.chart('graft_line', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Pengunjung' 
            },
            subtitle: {
                text: 'Dalam 1 Tahun Terakhir'
            },
            xAxis: {
                categories: <?php echo json_encode($sumbux_grafik_line); ?>,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah (orang)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} orang</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: <?php echo json_encode($data_grafik_line); ?>
        });

      </script>
      
    <?php if (empty($data_grafik)) { ?>

    <?php } else { ?>
      <?php foreach ($data_grafik as $dg) {
        $tanggal[] = intval($dg['tanggal']);
        $jumlah[] = intval($dg['jumlah']);
        $nama[] = $dg['namak'];
      } ?>
      <script>
            Highcharts.chart('graft', {
              title: {
                  text: ''
              },
              xAxis: {
                  categories: <?php echo json_encode($nama); ?>,
                  title: {
                    text: ''
                }
              },
              yAxis: {
                title: {
                  text: 'Jumlah'
                },
                tickInterval: 1
              },
              credits: {
                  enabled: false
              },
              plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
              },
              labels: {
                  items: [{
                      style: {
                          left: '50px',
                          top: '18px',
                          color: ( // theme
                              Highcharts.defaultOptions.title.style &&
                              Highcharts.defaultOptions.title.style.color
                          ) || 'black'
                      }
                  }]
              },
              series: [{
                  colorByPoint: true,
                  type: 'column',
                  name: 'Pengunjung',
                  data: <?php echo json_encode($jumlah); ?>
              }]
          });

          </script>
      <?php } ?>

    <!-- js untuk jquery -->
  <script src="js/jquery-1.11.2.min.js"></script>
  <!-- js untuk bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- js untuk moment -->
  <script src="js/moment.js"></script>
  <!-- js untuk bootstrap datetimepicker -->
  <script src="js/bootstrap-datetimepicker.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
         $("#btn_cari").submit();
       $('#tanggal').datetimepicker({
        format : 'DD/MM/YYYY'
       });
    });
  </script>
</body>