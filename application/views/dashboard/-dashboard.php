<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                Dashboard
                </h3>
                <div class="box-tools">
                    <a href="<?php echo site_url('dashboard/grafik'); ?>" class="btn btn-primary btn-sm">Grafik</a> 
                </div>
            </div>
            <div class="box-body">
                  <div class="row">
                  <div class="col-md-6">
                      <span>Dari</span>
                      <input type="date" name="waktu_dari" id="waktu_dari" value="<?php echo date('Y-m-d'); ?>"/>
                      <!-- <input type="date" name="waktu_dari" id="waktu_dari" value="2021-07-26"/> -->
                  </div>
                  <div class="col-md-12">
                      <span>Sampai</span>
                      <input type="date" name="waktu_sampai" id="waktu_sampai" value="<?php echo date('Y-m-d'); ?>"/>
                      <!-- <input type="date" name="waktu_sampai" id="waktu_sampai" value="2021-07-31"/> -->
                  </div>
                  <div class="col-md-12">
                      <span>Status</span>
                      <select name='id_status' id="id_status">
                        <option value='-1' selected>Semua<option>
                        <option value='<?php echo ID_STATUS_VAKSIN_HADIR; ?>' >Hadir<option>
                        <option value='<?php echo ID_STATUS_VAKSIN_TERDAFTAR; ?>' >Terdaftar<option>
                      </select>
                  </div>
                  <div class="col-md-12">
                      <span>Lokasi</span>
                      <select name='id_tempat_vaksin' id="id_tempat_vaksin">
                        <option value='-1' selected>Semua<option>
                        <?php foreach($tempat_vaksin as $teva){ ?>
                          <option value='<?php echo $teva['id']; ?>' ><?php echo $teva['nama']; ?><option>
                        <?php } ?>
                      </select>
                  </div>
                  <div class="col-md-12">
                      <button id='filter' class="btn btn-primary">Pilih</button>
                  </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info btn-info">
                        <div class="inner">
                            <h3 id='total_pendaftar'></h3>
                            <p>Permohonan Vaksin</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-group"></i>
                        </div>
                        <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
                        </div>
                    </div>
                    <!-- ./col -->
                    </div>
            </div>
            <div class="box-body">
                <p>*Catatan : Pencarian dengan NIK/NAMA/NO_HP/ALAMAT</p>
                <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                  <thead>
                    <tr>
                        <th>No</th>
                        <th>Tempat</th>
                        <th>Nik</th>
                        <th>Tanggal</th>
                        <th>Status Vaksin</th>
                        <th>Nama</th>
                        <th>No Hp</th>
                        <th>Alamat</th>
                    </tr>
                  </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  var table = $('#custom_datatable').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'print', {
                extend: 'pdfHtml5',
                
            orientation : 'landscape',
            pageSize : 'A3',
            }
    ],
    aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: 25,
    "processing": true,
    "serverSide": true,
    "order": [],

    "ajax": {
      "url": "<?php echo site_url('dashboard/get_data_permohonan_vaksin_json')?>",
       'data': function(d){
          d.waktu_dari = $('#waktu_dari').val(),
          d.waktu_sampai = $('#waktu_sampai').val(),
          d.id_status = $('#id_status option:selected').val(),
          d.id_tempat_vaksin = $('#id_tempat_vaksin option:selected').val()
      },
      "type": "POST",
      "dataSrc": function ( json ) {
        //Make your callback here.
        console.log(json.recordsTotal)
        $("#total_pendaftar").text(parseInt(json.recordsTotal))
        return json.data;
      }       

    },
    "columnDefs": [
      {
        "targets": [ 7 ],
        "orderable": false,
      },
      { "targets": 0 , "width": "5%" },
      { "targets": 1 , "width": "5%" },
      { "targets": 2 , "width": "10%" },
      { "targets": 3 , "width": "5%" },
      { "targets": 4 , "width": "5%" },
      { "targets": 5 , "width": "30%" },
      { "targets": 6 , "width": "5%" },
      { "targets": 7 , "width": "35%" },
    ],


  });
  $("#filter").click(function(){
    $('#custom_datatable').DataTable().ajax.reload();
  })

});

</script>