<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Angket Kepuasan</h3>
            	<div class="box-tools">
                    <!-- <a href="<?php echo site_url('skm/add'); ?>" class="btn btn-success btn-sm">Tambah</a>  -->
                </div>
            </div>
            <div class="box-body">
                <div class='row'>
                  <div class='col-md-6'>
                      <span>Dari</span>
                      <input type='date' name='waktu_dari' id='waktu_dari' value="<?php echo date('Y-m-d'); ?>" />
                  </div>
                  <div class='col-md-12'>
                      <span>Sampai</span>
                      <input type='date' name='waktu_sampai' id='waktu_sampai' value='<?php echo date('Y-m-d'); ?>' />
                  </div>
                  <div class='col-md-12'>
                      <span>Tempat</span>
                      <select name='id_tujuan' id='id_tujuan'>
                          <option value='-1' selected>Semua</option>
                          <?php foreach($all_tujuan as $tujuan){ ?>
                              <option value="<?php echo $tujuan['id']; ?>"><?php echo $tujuan['nama']; ?></option>
                          <?php } ?>
                      </select>
                  </div>
                  <div class='col-md-12'>
                      <button id='filter' class='btn btn-primary' >Pilih</button>
                  </div>
                </div>
                <table id="custom_datatable" class="display table-hover dt-responsive nowrap" width="100%">
                  <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Tempat</th>
                        <th>Penilaian</th>
                        <th>Saran</th>
						<th>Aksi</th>
                    </tr>
                  </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
  var table = $('#custom_datatable').DataTable({
    dom: 'lBfrtip',
    buttons:[
        'copy','csv','excel','print',{
            extend: 'pdfHtml5',
            orientation: 'landscape',
            pageSize:'A3',
        }
    ],
    iDisplayLength: 25,
    "processing": true, 
    "serverSide": true, 
    "order": [], 

    "ajax": {
      "url": "<?php echo site_url('skm/get_data_skm_json')?>",
      "type": "POST",
      'data':function(d){
          //console.log($('#id_tujuan').val())
          d.waktu_dari = $('#waktu_dari').val(),
          d.waktu_sampai = $('#waktu_sampai').val(),
          d.id_tujuan = $('#id_tujuan option:selected').val()
      },
      "type": "POST",
      'dataSrc': function(json){
          console.log(json.recordsTotal)
          return json.data
      }

    },
    "columnDefs": [
        { 
            "targets": [ 3,4], 
            "orderable": false, 
        },
        {'targets':0, 'width': '15%'},
        {'targets':1, 'width': '15%'},
        {'targets':2, 'width': '15%'},
        {'targets':3, 'width': '45%'},
        {'targets':4, 'width': '10%'},
    ],


  });
  $('#filter').click(function(){
      $('#custom_datatable').DataTable().ajax.reload();
  })
});

</script>

