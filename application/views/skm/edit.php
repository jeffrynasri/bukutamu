<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Ubah Angket Kepuasan</h3>
            </div>
			<?php echo form_open('skm/edit/'.$skm['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					
					<div class="col-md-12">
						<label for="tanggal" class="control-label">Tanggal</label>
						<div class="form-group">
							<input type="datetime-local" name="tanggal" value="<?php echo date('Y-m-d\TH:i:s', strtotime($skm['tanggal'])); ?>" class="form-control" id="tanggal" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="id_tujuan" class="control-label"><span class="text-danger">*</span>Tujuan</label>
						<div class="form-group">
							<select name="id_tujuan" class="form-control">
								<?php 
								foreach($all_tujuan as $tujuan)
								{
									$selected = ($tujuan['id'] == $kunjungan['id_tujuan']) ? ' selected="selected"' : "";

									echo '<option value="'.$tujuan['id'].'" '.$selected.'>'.$tujuan['nama'].'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('id_tujuan');?></span>
						</div>
					</div>
					<div class="col-md-12">
						<label for="saran" class="control-label">Saran</label>
						<div class="form-group">
							<input type="text" name="saran" value="<?php echo ($this->input->post('saran') ? $this->input->post('saran') : $skm['saran']); ?>" class="form-control" id="saran" />
						</div>
					</div>
					<div class="col-md-12">
						<label for="password_dewa" class="control-label"><span class="text-danger">*</span>Password</label>
						<div class="form-group">
							<input type="password" name="password_dewa" class="form-control" id="password_dewa" />
							<span class="text-danger"><?php echo form_error('password_dewa');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Simpan
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>