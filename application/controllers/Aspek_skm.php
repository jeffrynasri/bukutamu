<?php

include_once APPPATH . '/core/Admin_controller.php';
class Aspek_skm extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Aspek_skm_model');
        $this->load->model('Setting_model');
    } 

    /*
     * Listing of aspek_skm
     */
    function index()
    {
        $data['_view'] = 'aspek_skm/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';

        $data['_view'] = 'aspek_skm/index';
        $this->load->view('layouts/admin_template',$data);
    }

    function add()
    {   
        $this->load->library('form_validation');
		$this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('password_dewa','Password','required');
		
		if($this->form_validation->run())     
        {   
            $id="1";
            $data['setting'] = $this->Setting_model->get_setting($id);
            
            if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                $params = array(
                    'id' => str_replace("-","",$this->uuid->v4()),
                    'nama' => $this->input->post('nama'),
                );
                
                $user_id = $this->Aspek_skm_model->add_aspek_skm($params);
                redirect('aspek_skm/index');
            }else{
                show_error('password untuk mengubah/menghapus data salah');
            }
        }
        else
        {            
            $data['_view'] = 'aspek_skm/add';
            $data['_header'] = 'layouts/admin_header';
            $data['_sidebar'] = 'layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
    }  

    /*
     * Editing a aspek_skm
     */
    function edit($id)
    {   
        
        // check if the aspek_skm exists before trying to edit it
        $data['aspek_skm'] = $this->Aspek_skm_model->get_aspek_skm($id);
        
        if(isset($data['aspek_skm']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nama','Nama','required');
            $this->form_validation->set_rules('password_dewa','Password','required');
			
			if($this->form_validation->run())     
            {   
                $id_setting="1";
                $data['setting'] = $this->Setting_model->get_setting($id_setting);
                
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $params = array(
                        'nama' => $this->input->post('nama'),
                    );
    
                    $this->Aspek_skm_model->update_aspek_skm($id,$params);            
                    redirect('aspek_skm/index');
                }else{
                    show_error('password untuk mengubah/menghapus data salah');
                }
            }
            else
            {
                $data['_view'] = 'aspek_skm/edit';
                $data['_header']='layouts/admin_header';
                $data['_sidebar']='layouts/admin_sidebar';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The aspek_skm you are trying to edit does not exist.');
    } 

   
    /*
     * Deleting aspek_skm
     */
    function remove($id)
    {
        // check if the aspek_skm exists before trying to edit it
        $data['aspek_skm'] = $this->Aspek_skm_model->get_aspek_skm($id);
        
        if(isset($data['aspek_skm']['id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('password_dewa','Password','required');
			if($this->form_validation->run())     
            {   
                $id_setting="1";
                $data['setting'] = $this->Setting_model->get_setting($id_setting);
        
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $this->Aspek_skm_model->delete_aspek_skm($id);
                    redirect('aspek_skm/index');
                }else{
                    show_error('password untuk mengubah/menghapus data salah');
                }
            }
            $data['_view'] = 'aspek_skm/delete';
            $data['_header']='layouts/admin_header';
            $data['_sidebar']='layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
        else
            show_error('The aspek_skm you are trying to edit does not exist.');
    }
    function get_data_aspek_skm_json()
    {

        $list = $this->Aspek_skm_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $field->nama;
            $row[] = "<a href='" .  'edit/'. $field->id ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  'remove/'. $field->id ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Aspek_skm_model->count_all(),
            "recordsFiltered" => $this->Aspek_skm_model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
