<?php

include_once APPPATH . '/core/Admin_controller.php';
class Tujuan extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tujuan_model');
        $this->load->model('Setting_model');
    } 

    /*
     * Listing of tujuan
     */
    function index()
    {
        $data['_view'] = 'tujuan/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';

        $data['_view'] = 'tujuan/index';
        $this->load->view('layouts/admin_template',$data);
    }

    function add()
    {   
        $this->load->library('form_validation');
		$this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('password_dewa','Password','required');
		
		if($this->form_validation->run())     
        {   
            $id="1";
            $data['setting'] = $this->Setting_model->get_setting($id);
            
            if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                $params = array(
                    'id' => str_replace("-","",$this->uuid->v4()),
                    'nama' => $this->input->post('nama'),
                );
                
                $user_id = $this->Tujuan_model->add_tujuan($params);
                redirect('tujuan/index');
            }else{
                show_error('password untuk mengubah/menghapus data salah');
            }
        }
        else
        {            
            $data['_view'] = 'tujuan/add';
            $data['_header'] = 'layouts/admin_header';
            $data['_sidebar'] = 'layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
    }  

    /*
     * Editing a tujuan
     */
    function edit($id)
    {   
        
        // check if the tujuan exists before trying to edit it
        $data['tujuan'] = $this->Tujuan_model->get_tujuan($id);
        
        if(isset($data['tujuan']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nama','Nama','required');
            $this->form_validation->set_rules('password_dewa','Password','required');
			
			if($this->form_validation->run())     
            {   
                $id_setting="1";
                $data['setting'] = $this->Setting_model->get_setting($id_setting);
                
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $params = array(
                        'nama' => $this->input->post('nama'),
                    );
    
                    $this->Tujuan_model->update_tujuan($id,$params);            
                    redirect('tujuan/index');
                }else{
                    show_error('password untuk mengubah/menghapus data salah');
                }
            }
            else
            {
                $data['_view'] = 'tujuan/edit';
                $data['_header']='layouts/admin_header';
                $data['_sidebar']='layouts/admin_sidebar';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The tujuan you are trying to edit does not exist.');
    } 

   
    /*
     * Deleting tujuan
     */
    function remove($id)
    {
        // check if the tujuan exists before trying to edit it
        $data['tujuan'] = $this->Tujuan_model->get_tujuan($id);
        
        if(isset($data['tujuan']['id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('password_dewa','Password','required');
			if($this->form_validation->run())     
            {   
                $id_setting="1";
                $data['setting'] = $this->Setting_model->get_setting($id_setting);
        
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $this->Tujuan_model->delete_tujuan($id);
                    redirect('tujuan/index');
                }else{
                    show_error('password untuk mengubah/menghapus data salah');
                }
            }
            $data['_view'] = 'tujuan/delete';
            $data['_header']='layouts/admin_header';
            $data['_sidebar']='layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
        else
            show_error('The tujuan you are trying to edit does not exist.');
    }
    function get_data_tujuan_json()
    {

        $list = $this->Tujuan_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $field->nama;
            $row[] = "<a href='" .  'edit/'. $field->id ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  'remove/'. $field->id ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Tujuan_model->count_all(),
            "recordsFiltered" => $this->Tujuan_model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
