<?php

Class Webcam extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data['_view']='home/webcam';
        $data['_footer']='layouts/home_footer';
        $data['_header']='layouts/home_header';
        $this->load->view('layouts/home_template',$data);
    }

    function simpan(){
        $image = $this->input->post('image');
        $image = str_replace('data:image/jpeg;base64,','',$image);
        $image = base64_decode($image);
        file_put_contents(FCPATH.'/assets/bleble.jpg',$image);
    }
}