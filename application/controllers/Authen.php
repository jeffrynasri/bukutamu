<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Authen extends CI_Controller {
	private $ACTKEY_LOGIN = 'login';
	private $ACTKEY_LOGOUT = 'logout';
	private $ACTKEY_FORGETPASSWORD = 'forgetpass';

	function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->library('session');
		$this->load->helper('string');
		$this->load->helper('password_helper');
		$this->load->model("User_model");
		$this->load->model('Loguser_model');
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index(){

		$this->form_validation->set_rules('uname', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if($this->form_validation->run()){
			$uname= $this->input->post('uname');
			$password = $this->input->post('password');
			$status_login=$this->is_registered($uname,$password);
			if(strlen($status_login) > 0){
				$this->login_success($uname,$status_login);
			}else{
				$this->login_failed();
			}
		}else{
			$data['_view']='login';
			$this->load->view('layouts/home_template',$data);
		}
	}
	public function logout(){
		$uname= $this->session->userdata(SESSION_LOGIN_USERNAME);
		$userid=$this->session->userdata(SESSION_LOGIN_USERID);
		$this->session->sess_destroy();
		$this->logact(
			array(
				'userid' => $userid,
				'logact' => $this->ACTKEY_LOGOUT,
				'logdate' => date('Y-m-d H:i:s'),
				'logip' => $this->input->ip_address(),
				'logplatagent' => $this->cekPlatAgent()
			)
		);
		redirect('authen/index');
	}

	private function is_registered($username,$pass){
		$user = $this->User_model->get_user($username);
		//print_r($user);
		if($user){
			if(password_verify($pass, $user['pass'])) {
				if($user['ustate']=='1'){
					return KEY_LOGIN_ADMIN;
				}
			}
		}

		return "";
	}

	private function login_success($uname,$status_login){
		if($status_login == KEY_LOGIN_ADMIN){
			$user=$this->User_model->get_user($uname);
		}

		$newdata = [
			SESSION_LOGIN_USERID  => $user['userid'],
			SESSION_LOGIN_USERNAME     => $uname,
			SESSION_LOGIN_DESKRIPSI     => $user['deskripsi'],
			SESSION_LOGIN_NAMA     => $user['nama'],
			SESSION_LOGIN_LOGGEDIN => TRUE
		];
		$this->session->set_userdata($newdata);
		$this->logact(
			array(
				'userid' => $user['userid'],
				'logact' => $this->ACTKEY_LOGIN,
				'logdate' => date('Y-m-d H:i:s'),
				'logip' => $this->input->ip_address(),
				'logplatagent' => $this->cekPlatAgent()
			)
		);
		if($status_login == KEY_LOGIN_ADMIN){
			redirect('dashboard/index');
			//redirect('permohonan_vaksin/index');
		}
	}
	private function login_failed(){
		$data['_view']='login';
		$this->session->set_flashdata('pesan','Login Gagal');
		$this->load->view('layouts/home_template',$data);
	}



	private function logact($data){
		return $this->Loguser_model->add_loguser($data);
	}
	private function cekPlatAgent()
	{
		if ($this->agent->is_browser()) {
			$agent = $this->agent->browser().' '.$this->agent->version();
		}
		elseif ($this->agent->is_robot()) {
			$agent = $this->agent->robot();
		}
		elseif ($this->agent->is_mobile()) {
			$agent = $this->agent->mobile();
		}
		else {
			$agent = 'Unidentified User Agent';
		}
		$platformAgent = '('.$this->agent->platform().') '.$agent; // Platform info (Windows, Linux, Mac, etc.)
		return $platformAgent;
	}
     private function generate_password_rah45ia(){
        //echo hash_password("terasa31");
		//echo hash_password("terasa32");
		echo hash_password("melon_hijau");
		//echo hash_password("admin22");
    }
	private function bulk_admin(){
		//vaksinsehat
		//semoga_lancar
		for($i=1;$i<=20;$i++){
			$params = array(
				'userid' => str_replace("-","",$this->uuid->v4()),
                'nama' => "Admin ".$i,
                'email' => "",
                'pass' => hash_password("semoga_lancar".$i),
                'uname' => "vaksin_sehat".$i,
                'deskripsi' => "",
                'ustate' => "1",
                'ucreated' => date('Y-m-d H:i:s'),
                'umodified' => date('Y-m-d H:i:s'),
                'ucreatedby' => "Hulk",
                'umodifiedby' => "Hulk",
            );
            
            $user_id = $this->User_model->add_user($params);
		}
		echo "selesai";
    }

}
