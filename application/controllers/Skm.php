<?php

 include_once APPPATH . '/core/Admin_controller.php';
class Skm extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Skm_model');
        $this->load->model('Aspek_skm_model');
        $this->load->model('Penilaian_model');
        $this->load->model('Tujuan_model');
        $this->load->model('Setting_model');
    } 

    /*
     * Listing of skm
     */
    function index()
    {
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $data['all_tujuan'] = $this->Tujuan_model->get_all_tujuan();
        $data['_view'] = 'skm/index';
        $this->load->view('layouts/admin_template',$data);
    }

   


    /*
     * Editing a skm
     */
    function edit($id)
    {   
        // check if the skm exists before trying to edit it
        $data['skm'] = $this->Skm_model->get_skm($id);
        
        if(isset($data['skm']['id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('tanggal','Tanggal','required');
			$this->form_validation->set_rules('id_tujuan','Id Tujuan','required');
		
			if($this->form_validation->run())     
            {   
                $id_setting="1";
                $data['setting'] = $this->Setting_model->get_setting($id_setting);
                
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $params = array(
                        'id_tujuan' => $this->input->post('id_tujuan'),
                        'tanggal' => $this->input->post('tanggal'),
                        'saran' => $this->input->post('saran'),
                    );
    
                    $this->Skm_model->update_skm($id,$params);            
                    redirect('skm/index');
                }else{
                    show_error('password untuk mengubah/menghapus data salah');
                }
            }
            else
            {
				$this->load->model('Tujuan_model');
				$data['all_tujuan'] = $this->Tujuan_model->get_all_tujuan();
                $data['_header']='layouts/admin_header';
                $data['_sidebar']='layouts/admin_sidebar';
                $data['_view'] = 'skm/edit';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The skm you are trying to edit does not exist.');
    } 

    /*
     * Deleting skm
     */
    function remove($id)
    {
        $skm = $this->Skm_model->get_skm($id);

        // check if the skm exists before trying to delete it
        if(isset($skm['id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('password_dewa','Password','required');
			if($this->form_validation->run())     
            {   
                $id_setting="1";
                $data['setting'] = $this->Setting_model->get_setting($id_setting);
        
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $this->Penilaian_model->delete_penilaian($id);
                    $this->Skm_model->delete_skm($id);
                    redirect('skm/index');
                }else{
                    show_error('password untuk mengubah/menghapus data salah');
                }
            }
            $data['skm'] = $skm;
            $data['_view'] = 'skm/delete';
            $data['_header']='layouts/admin_header';
            $data['_sidebar']='layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
        else
            show_error('The skm you are trying to delete does not exist.');
    }

    function get_data_skm_json()
    {
        
        $params_where=array(
            'tanggal >=' => $this->input->post('waktu_dari'),
            'tanggal <=' => date('Y-m-d',strtotime($this->input->post('waktu_sampai').' +1 day'))
        );
        if($this->input->post('id_tujuan')!='-1'){
            $params_where['id_tujuan']=$this->input->post('id_tujuan');
        }
        
        $list = $this->Skm_model->get_datatables($params_where);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $field->tanggal;
            $row[] = $field->tujuan_nama;
            $penilaian = $this->Penilaian_model->get_penilaian_byskm($field->id);
            $str_penilaian="<ul>";
            foreach($penilaian as $pnl){
                $str_penilaian=$str_penilaian.'<li>'.$pnl['nama'] ." : ".$pnl['nilai'].'</li>';    
            }
            $str_penilaian=$str_penilaian."</ul>";
            $row[] = $str_penilaian;
            $row[] = $field->saran;
            $row[] = "<a href='" .  'edit/'. $field->id ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  'remove/'. $field->id ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";
            $data[] = $row;
        }
        //echo $this->Skm_model->count_all($params_where);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Skm_model->count_all($params_where),
            "recordsFiltered" => $this->Skm_model->count_filtered($params_where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
