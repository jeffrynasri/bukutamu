<?php
      
class Skm extends CI_Controller{
    public $CI = NULL;

    function __construct()
    {
        parent::__construct();
        $this->load->library('session'); 
        if(!$this->session->has_userdata('nama_adminn')){
            redirect('admin');   
        }
        $this->CI = & get_instance();
        $this->load->model('Skm_model');
        $this->load->model('Penilaian_model');
        
    } 

    /*
     * Listing of skm
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('skm/index?');
        $config['total_rows'] = $this->Skm_model->get_all_skm_count();
        $this->pagination->initialize($config);

        $data['skm'] = $this->Skm_model->get_all_skm($params);
        $data['_view'] = 'skm/index';
        $this->load->view('layouts/main',$data);
    }

    public function get_penilaian($id_skm){
        return $this->Skm_model->get_penilaian_byskm($id_skm);
    }
    /*
     * Adding a new skm
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'saran' => $this->input->post('saran'),
				'tanggal' => $this->input->post('tanggal'),
            );
            
            $skm_id = $this->Skm_model->add_skm($params);
            redirect('skm/index');
        }
        else
        {            
            $data['_view'] = 'skm/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a skm
     */
    function edit($id)
    {   
        // check if the skm exists before trying to edit it
        $data['skm'] = $this->Skm_model->get_skm($id);
        
        if(isset($data['skm']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'saran' => $this->input->post('saran'),
					'tanggal' => $this->input->post('tanggal'),
                );

                $this->Skm_model->update_skm($id,$params);            
                redirect('skm/index');
            }
            else
            {
                $data['_view'] = 'skm/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The skm you are trying to edit does not exist.');
    } 

    /*
     * Deleting skm
     */
    function remove($id)
    {
        $skm = $this->Skm_model->get_skm($id);

        // check if the skm exists before trying to delete it
        if(isset($skm['id']))
        {
            $this->Penilaian_model->delete_penilaian_byidskm($id);
            $this->Skm_model->delete_skm($id);
            redirect('skm/index');
        }
        else
            show_error('The skm you are trying to delete does not exist.');
    }
    
}
