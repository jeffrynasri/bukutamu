<?php

class Dashboard extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kunjungan_model');
        $this->load->model('Tujuan_model');
        $this->load->model('Layanan_model');
       
    }

    function index()
    {
        $params_where=array();
    	if(isset($_POST) && count($_POST) > 0){   
            if($this->input->post('id_tujuan')!='-1'){
                $params_where['id_tujuan']=$this->input->post('id_tujuan');
            }
        }
        $tahun_sekarang = date('Y', time());
        $bulan_sekarang = date('m', time());
       	$array_bulan = [1,2,3,4,5,6,7,8,9,10,11,12];
        $array_bulan_string=array();
        $array_layanan=$this->Layanan_model->get_all_layanan();
        
        $arrayresult = array();

        //**************************ALGORITMA UNTUK GRAFIK LINE**********************
        foreach ($array_bulan as $bulan) {
            array_push($array_bulan_string, date('F', mktime(0, 0, 0, $bulan, 10))) ;
        }

        foreach($array_layanan as $layanan){
            $array_jumlahdata = [];
            for($j =0 ; $j< count($array_bulan); $j++){
                $params_where['month(tanggal)'] = $array_bulan[$j];
                $params_where['year(tanggal)'] = $tahun_sekarang;
                $params_where['id_layanan'] = $layanan['id'];
                $jumlahdata = count($this->Kunjungan_model->get_all_kunjungan($params_where));
                //$jumlahdata = $this->Kunjungan_model->get_report_kunjungan_permonth_bynama($array_bulan[$j], $tahun_sekarang, $array_penilaian[$i])[0]['jumlah'];
                array_push($array_jumlahdata, (int)$jumlahdata);
            }

			
            $array_obj= array(
                'name' => $layanan['nama'], 
                'data' => $array_jumlahdata);
            array_push($arrayresult, $array_obj);
        }

        $data['data_grafik_line'] = $arrayresult;
        $data['sumbux_grafik_line'] = $array_bulan_string;
        $data['all_tujuan'] =$this->Tujuan_model->get_all_tujuan();
        $data['_view'] = 'dashboard/dashboard';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar']='layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);    

    }


    // function convert_bilnilai_ke_warna($nilai){
    //     $return = "";
    //     switch ($nilai) {
    //         case "LPSE":
    //             $return = "#0275d8";
    //             break;
    //         case "Bidang":
    //             $return = "#5cb85c";
    //             break;
    //         case "Sekretariat":
    //             $return = "#d9534f";
    //             break;
    //         case "Pokja":
    //             $return = "#5bc0de";
    //             break;
    //         case "umum":
    //             $return = "#f0ad4e";
    //             break;
    //         default:
    //             $return = "#f0ad4e";
    //             break;
    //     }
    //     return $return;
    // }
}
