<?php

include_once APPPATH. '/core/Admin_controller.php';
class Dashboard_skm extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('date_helper');

        $this->load->model('Penilaian_model');
        $this->load->model('Skm_model');
        $this->load->model('Aspek_skm_model');
        $this->load->model('Tujuan_model');
    }

    function index()
    {
        $params_where=array();
        if(isset($_POST)&&count($_POST)>0){
            if($this->input->post('id_tujuan')!='-1'){
                $params_where['id_tujuan']=$this->input->post('id_tujuan');
            }
        }

    	$tahun_sekarang = date('Y', time());
        $bulan_sekarang = date('m', time());
        $array_empat_bulan_lalu_string=[];
        $array_empat_bulan_lalu = get_empat_bulan_yglalu($bulan_sekarang);
        $array_empat_tahun_lalu = get_empat_tahun_yglalu($tahun_sekarang, $array_empat_bulan_lalu);
        $array_penilaian = [1,2,3];
        $arrayresult = array();

        //**************************ALGORITMA UNTUK GRAFIK LINE**********************
        foreach ($array_empat_bulan_lalu as $empat_bulan_lalu) {
            array_push($array_empat_bulan_lalu_string, date('F', mktime(0, 0, 0, $empat_bulan_lalu, 10))) ;
        }
        for($i=0;$i<count($array_penilaian);$i++){
            $array_jumlahdata = [];
            for($j =0 ; $j< count($array_empat_tahun_lalu); $j++){
                //$jumlahdata = $this->Penilaian_model->get_report_survey_permonthbynilai($array_empat_bulan_lalu[$j], $array_empat_tahun_lalu[$j], $array_penilaian[$i])[0]['jumlah'];
                //array_push($array_jumlahdata, (int)$jumlahdata);
                $jumlahdata = $this->Penilaian_model->get_report_survey_permonthbynilai($array_empat_bulan_lalu[$j], $array_empat_tahun_lalu[$j], $array_penilaian[$i],$params_where);
                array_push($array_jumlahdata, count($jumlahdata));
            }


            $array_obj= array(
                'name' => $this->convert_bilnilai_ke_namanilai($array_penilaian[$i]), 
                'color' => $this->convert_bilnilai_ke_warna($array_penilaian[$i]), 
                'data' => $array_jumlahdata);
            array_push($arrayresult, $array_obj);
        }

        //**************************ALGORITMA UNTUK GRAFIK PIE**********************
        $penilaian_groupbynilai = $this->Penilaian_model->get_penilaian_groupbyilai($params_where);
        $penilaian_groupbynilai_detail=array();
        $arrayresult_pie = array();
        $isselected=0;
        foreach ($penilaian_groupbynilai as $pgn) {
            //*********Algo Detail penilaian group by nilai */
            $detialed_penilaian = $this->Penilaian_model->get_detail_penilaian_groupbynilai($pgn['nilai'],$params_where);
            array_push($penilaian_groupbynilai_detail,array(
                'nilai' => $pgn['nilai'],
                'result' => $detialed_penilaian
            ));
            //*********End - Algo Detail penilaian group by nilai */
            if($isselected<1){
                $array_obj= array(
                    'name' => $this->convert_bilnilai_ke_namanilai($pgn['nilai']), 
                    'y' => (int)$pgn['jumlah'],
                    'color' => $this->convert_bilnilai_ke_warna($pgn['nilai']), 
                    'sliced'=> true,
                    'selected' => true);
            }else{
                $array_obj= array(
                    'name' => $this->convert_bilnilai_ke_namanilai($pgn['nilai']),
                    'color' => $this->convert_bilnilai_ke_warna($pgn['nilai']), 
                    'y' => (int)$pgn['jumlah']);   
            }


            array_push($arrayresult_pie, $array_obj);
            $isselected=1;
        }
        $data['all_tujuan']=$this->Tujuan_model->get_all_tujuan();
        $data['jumlah_koresponden']= sizeof($this->Skm_model->get_all_skm($params_where));
        $data['jumlah_aspek_skm']= sizeof($this->Aspek_skm_model->get_all_aspek_skm());
        $data['data_grafik_pie'] = $arrayresult_pie;
        //$data['tabel_grafik_pie'] = $penilaian_groupbynilai;
        $data['tabel_grafik_pie'] = $penilaian_groupbynilai_detail;
        $data['data_grafik'] = $arrayresult;
        $data['sumbux_grafik'] = $array_empat_bulan_lalu_string;
        $data['_view'] = 'dashboard/dashboard_skm';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        //echo json_encode($penilaian_groupbynilai_detail);
        $this->load->view('layouts/admin_template',$data);    
        
        
    }
    function convert_bilnilai_ke_namanilai($nilai){
        $return = "";
        switch ($nilai) {
            case 1:
                $return = "Kurang";
                break;
            case 2:
                $return = "Cukup";
                break;
            case 3:
                $return = "Baik";
                break;
            default:
                $return = "Unknows";
                break;
        }
        return $return;
    }

    function convert_bilnilai_ke_warna($nilai){
        $return = "";
        switch ($nilai) {
            case 1:
                $return = "#d9534f";
                break;
            case 2:
                $return = "#f0ad4e";
                break;
            case 3:
                $return = "#5cb85c";
                break;
            default:
                $return = "#5cb85c";
                break;
        }
        return $return;
    }

    
}
