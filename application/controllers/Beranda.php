<?php

class Beranda extends CI_Controller{
    function __construct()
    {
        parent::__construct();
		$this->load->model('Kunjungan_model');
        $this->load->library('session');        
        $this->load->model("Aspek_skm_model");
        $this->load->model("Penilaian_model");
        $this->load->model('Tujuan_model');
        $this->load->model('Layanan_model');
        $this->load->model("Skm_model");
        $this->load->model("Setting_model");
        date_default_timezone_set('Asia/Bangkok');
    }
    function testing(){
        $ch = curl_init();
        //TES PDN
        //****
       curl_setopt($ch, CURLOPT_URL, 'https://testing-server.gresikkab.go.id/dashboard/test_api');
       // curl_setopt($ch, CURLOPT_URL, 'http://10.90.250.36/dashboard/test_api');
        //******
        //curl_setopt($ch, CURLOPT_URL, 'http://103.170.105.146/testing.php');
        //  curl_setopt($ch, CURLOPT_URL, 'http://10.90.250.10/testing.php');
        
        //TES NON PDN
       //curl_setopt($ch, CURLOPT_URL, 'https://dummyjson.com/products/1');
       //curl_setopt($ch, CURLOPT_URL, '103.142.210.143/testing.php');
       
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output=curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            echo $error_msg;
        }
        echo $output;
        curl_close($ch);
        
        if (isset($error_msg)) {
            // TODO - Handle cURL error accordingly
        }
    }
    function index()
    {
        
        $this->load->library('form_validation');

        $this->form_validation->set_rules('no_hp','No Hp','required|integer');
        $this->form_validation->set_rules('nama','Nama','required');
        //$this->form_validation->set_rules('keterangan','Keterangan','required');
        $this->form_validation->set_rules('instansi','Instansi','required');
        $this->form_validation->set_rules('id_tujuan','Tujuan','required');
        $this->form_validation->set_rules('id_layanan','Layanan','required');

        if($this->form_validation->run())     
        {   
            date_default_timezone_set("Asia/Bangkok");
            $params = array(
                'id' => str_replace("-","",$this->uuid->v4()),
                'id_layanan' => $this->input->post('id_layanan'),
                'id_tujuan' => $this->input->post('id_tujuan'),
                'tanggal' => date('Y-m-d H:i:s'),
                'nama' => $this->input->post('nama'),
                'no_hp' => $this->input->post('no_hp'),
                'keterangan' => $this->input->post('keterangan'),
                'instansi' => $this->input->post('instansi'),
                'email' => $this->input->post('email')
            );
            
            
            $kunjungan_id = $this->Kunjungan_model->add_kunjungan($params);
            $pesan_pendaftaran = '';
            if($kunjungan_id>0){
                $pesan_pendaftaran='Pengisian Berhasil';
            }else{
                $pesan_pendaftaran='Pengisian GAGAL';
            }
            
            $this->session->set_flashdata('pesan',$pesan_pendaftaran);
            redirect('beranda/index');
            // $data['all_tujuan'] = $this->Tujuan_model->get_all_tujuan();
            // $data['all_aspek_skm'] = $this->Aspek_skm_model->get_all_aspek_skm();
            // unset ($_POST);
            // $this->load->view('home/bukutamu',$data);
        }
        else
        {
            $data['_view']='home/bukutamu';
            $data['setting'] = $this->Setting_model->get_setting('1');
            $data['all_layanan'] = $this->Layanan_model->get_all_layanan();
            $data['all_tujuan'] = $this->Tujuan_model->get_all_tujuan();
            $data['all_aspek_skm'] = $this->Aspek_skm_model->get_all_aspek_skm();
            $this->load->view('layouts/home_template',$data);
        }
    }
    
    function skm(){
        // $this->load->library('form_validation');  
        // $this->form_validation->set_rules('saran',"Saran",'required');
        if(isset($_POST) && count($_POST) > 0){
            $skm_id = str_replace('-','',$this->uuid->v4());
            $params_skm = array(
                'id' => $skm_id,
                'saran' => $this->input->post('saran'),
                'tanggal' => date('Y-m-d H:i:s'),
                'id_tujuan' => $this->input->post('id_tujuan'),
            );
            $skm_return = $this->Skm_model->add_skm($params_skm);
            $all_aspek_skm = $this->Aspek_skm_model->get_all_aspek_skm();
            foreach ($all_aspek_skm as $askm) {
                $params_penilaian = array(
                    'nilai' => $this->input->post(str_replace(" ","-",$askm['nama'])),
                    'id_skm' => $skm_id,
                    'id_aspek_skm' => $askm['id'],
                );
                $id_penilaian= $this->Penilaian_model->add_penilaian($params_penilaian);
            }
            $this->session->set_flashdata('pesan','Input Data Berhasil');
            redirect('beranda/skm');
        }else{
            $data['all_tujuan']=$this->Tujuan_model->get_all_tujuan();
            $data['_view']='home/skm';
            $data['setting']=$this->Setting_model->get_setting('1');
            $data['all_aspek_skm']=$this->Aspek_skm_model->get_all_aspek_skm();
            $this->load->view('layouts/home_template',$data);
        }
    }
    function pulang(){
        // $this->load->library('form_validation');  
        // $this->form_validation->set_rules('saran',"Saran",'required');
        if(isset($_POST) && count($_POST) > 0){
            $id= $this->input->post('id_kunjungan');
            $params = array(
                'pulang' => date('Y-m-d H:i:s'),
            );
            $this->Kunjungan_model->update_kunjungan($id,$params);            
            $this->session->set_flashdata('pesan','Input Data Berhasil');
            redirect('beranda/index');
        }else{
            $data['all_kunjungan']=$this->Kunjungan_model->get_kunjungan_not_pulang();
            $data['_view']='home/pulang';
            $data['setting']=$this->Setting_model->get_setting('1');
            $this->load->view('layouts/home_template',$data);
        }
    }

    ///////////-------AUTOCMPLETE--------------
    function get_autocomplete_name(){
        if (isset($_GET['term'])) {
            $result = $this->Kunjungan_model->search_name($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->nama;
                echo json_encode($arr_result);
            }
        }
    }
    function get_autocomplete_nohp(){
        if (isset($_GET['term'])) {
            $result = $this->Kunjungan_model->search_nohp($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->no_hp;
                echo json_encode($arr_result);
            }
        }
    }
    function get_autocomplete_keterangan(){
        if (isset($_GET['term'])) {
            $result = $this->Kunjungan_model->search_keterangan($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->keterangan;
                echo json_encode($arr_result);
            }
        }
    }
    function get_autocomplete_instansi(){
        if (isset($_GET['term'])) {
            $result = $this->Kunjungan_model->search_instansi($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->instansi;
                echo json_encode($arr_result);
            }
        }
    }
    function get_autocomplete_email(){
        if (isset($_GET['term'])) {
            $result = $this->Kunjungan_model->search_email($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->email;
                echo json_encode($arr_result);
            }
        }
    }
    ///////////-------End Of AUTOCMPLETE
}

