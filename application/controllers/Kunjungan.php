<?php

 include_once APPPATH . '/core/Admin_controller.php';
class Kunjungan extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kunjungan_model');
        $this->load->model('Tujuan_model');
        $this->load->model('Layanan_model');
        $this->load->model('Setting_model');
    } 

    /*
     * Listing of kunjungan
     */
    function index()
    {
        $data['all_tujuan']=$this->Tujuan_model->get_all_tujuan();
        $data['all_layanan']=$this->Layanan_model->get_all_layanan();
        $data['_view'] = 'kunjungan/index';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';

        $data['_view'] = 'kunjungan/index';
        $this->load->view('layouts/admin_template',$data);
    }

   


    /*
     * Editing a kunjungan
     */
    function edit($id)
    {   
        // check if the kunjungan exists before trying to edit it
        $data['kunjungan'] = $this->Kunjungan_model->get_kunjungan($id);
        
        if(isset($data['kunjungan']['id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('no_hp','No Hp','required|integer');
			$this->form_validation->set_rules('nama','Nama','required');
            $this->form_validation->set_rules('instansi','Instansi','required');
			$this->form_validation->set_rules('tanggal','Tanggal','required');
			$this->form_validation->set_rules('id_tujuan','Id Tujuan','required');
            $this->form_validation->set_rules('id_layanan','Id Layanan','required');
		
			if($this->form_validation->run())     
            {   
                $id_setting="1";
                $data['setting'] = $this->Setting_model->get_setting($id_setting);
                
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $params = array(
                        'id_tujuan' => $this->input->post('id_tujuan'),
                        'id_layanan' => $this->input->post('id_layanan'),
                        'tanggal' => $this->input->post('tanggal'),
                        'nama' => $this->input->post('nama'),
                        'no_hp' => $this->input->post('no_hp'),
                        'keterangan' => $this->input->post('keterangan'),
                        'instansi' => $this->input->post('instansi'),
                        'email' => $this->input->post('email'),
                    );
    
                    $this->Kunjungan_model->update_kunjungan($id,$params);            
                    redirect('kunjungan/index');
                }else{
                    show_error('password untuk mengubah/menghapus data salah');
                }
            }
            else
            {
				$this->load->model('Tujuan_model');
				$data['all_tujuan'] = $this->Tujuan_model->get_all_tujuan();
                $data['all_layanan'] = $this->Layanan_model->get_all_layanan();
                $data['_header'] = 'layouts/admin_header';
                $data['_sidebar'] = 'layouts/admin_sidebar';
                $data['_view'] = 'kunjungan/edit';
                $this->load->view('layouts/admin_template',$data);
            }
        }
        else
            show_error('The kunjungan you are trying to edit does not exist.');
    } 

    /*
     * Deleting kunjungan
     */
    function remove($id)
    {
        $kunjungan = $this->Kunjungan_model->get_kunjungan($id);

        // check if the kunjungan exists before trying to delete it
        if(isset($kunjungan['id']))
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('password_dewa','Password','required');
			if($this->form_validation->run())     
            {   
                $id_setting="1";
                $data['setting'] = $this->Setting_model->get_setting($id_setting);
        
                if(password_verify($this->input->post('password_dewa'), $data['setting']['password_dewa'])) {
                    $this->Kunjungan_model->delete_kunjungan($id);
                    redirect('kunjungan/index');
                }else{
                    show_error('password untuk mengubah/menghapus data salah');
                }                
            }
            $data['kunjungan'] = $kunjungan;
            $data['_view'] = 'kunjungan/delete';
            $data['_header']='layouts/admin_header';
            $data['_sidebar']='layouts/admin_sidebar';
            $this->load->view('layouts/admin_template',$data);
        }
        else
            show_error('The kunjungan you are trying to delete does not exist.');
    }

    function get_data_kunjungan_json()
    {
        // $params_where=array(
        //     'tanggal >=' => $this->input->post('waktu_dari')
        // );

        $params_where=array(
            'tanggal >=' => $this->input->post('waktu_dari'),
            'tanggal <=' => date('Y-m-d',strtotime($this->input->post('waktu_sampai').' +1 day'))
        );
        if($this->input->post('id_tujuan')!='-1'){
            $params_where['id_tujuan']=$this->input->post('id_tujuan');
        }
        if($this->input->post('id_layanan')!='-1'){
            $params_where['id_layanan'] = $this->input->post('id_layanan');
        }
        
        $list = $this->Kunjungan_model->get_datatables($params_where);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = "Datang : " . $field->tanggal. "<br>". "Pulang : " . $field->pulang;
            $row[] = $field->nama;
            $row[] = $field->no_hp;
            $row[] = $field->instansi;
            $row[] = $field->tujuan_nama;
            $row[] = $field->layanan_nama;  
            $row[] = $field->email;
            $row[] = $field->keterangan; 
            $row[] = "<a href='" .  'edit/'. $field->id ."'" . "class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ubah</a>" .
            "<br>" .
            "<a href='" .  'remove/'. $field->id ."'" . "class='btn btn-danger btn-xs' onclick='return confirm(\" Apakah Anda Yakin Akan Menghapus Data Ini ? \")'><span class='fa fa-trash'></span> Hapus</a>";

            $data[] = $row;
        }
        //echo $this->Kunjungan_model->count_all($params_where);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Kunjungan_model->count_all($params_where),
            "recordsFiltered" => $this->Kunjungan_model->count_filtered($params_where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
