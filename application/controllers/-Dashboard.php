<?php
include_once APPPATH .'/core/Admin_controller.php';

class Dashboard extends Admin_controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Permohonan_vaksin_model");
        $this->load->model('Tempat_vaksin_model');
    }

    function index()
    {
        $data['tempat_vaksin']=$this->Tempat_vaksin_model->get_all_tempat_vaksin();
        $data['_view'] = 'dashboard/dashboard';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);
    }
    function grafik(){
        $data['tempat_vaksin']=$this->Tempat_vaksin_model->get_all_tempat_vaksin();
        $this->form_validation->set_rules('waktu_dari','Waktu','required');
        if($this->form_validation->run()){  
            $waktuawal=$this->input->post('waktu_dari');
            $waktuakhir=$this->input->post('waktu_sampai');
            $axis_grafik=array();
            $value_grafik_hadir=array();
            $value_grafik_terdaftar=array();
            for($i=$waktuawal;$i<=$waktuakhir;){
                array_push($axis_grafik,date('m-d-Y',strtotime($i)));
                $params_where=array(
                    'tanggal_permohonan'=>$i
                );
                if($this->input->post('id_tempat_vaksin')!='-1'){
                    $params_where['id_tempat_vaksin']=$this->input->post('id_tempat_vaksin');
                }
                $params_where["id_status"]=ID_STATUS_VAKSIN_HADIR;
                $total_hadir=$list = count($this->Permohonan_vaksin_model->get_all_permohonan_vaksin($params_where));
                $params_where["id_status"]=ID_STATUS_VAKSIN_TERDAFTAR;
                $total_terdaftar=count($this->Permohonan_vaksin_model->get_all_permohonan_vaksin($params_where));
                array_push($value_grafik_hadir,(int)$total_hadir);
                array_push($value_grafik_terdaftar,(int)$total_terdaftar);
                $i=date('Y-m-d', strtotime($i. ' + 1 days'));
            }
            $final_value=array(
                array(
                    'name'=> 'Hadir',
                    'data'=>$value_grafik_hadir
                ),
                array(
                    'name'=> 'Terdaftar',
                    'data'=>$value_grafik_terdaftar
                ),
            );
            //echo json_encode($axis_grafik);
            //echo json_encode($final_value);
            $data['xasix'] = $axis_grafik;
            $data['yasix'] = $final_value;
        }else{
            $data['xasix'] = array();
        }
        $data['_view'] = 'dashboard/grafik';
        $data['_header'] = 'layouts/admin_header';
        $data['_sidebar'] = 'layouts/admin_sidebar';
        $this->load->view('layouts/admin_template',$data);
    }
    function get_data_permohonan_vaksin_json()
    {
        //$waktu_explode=explode("-",$this->input->post('waktu'));
        $params_where=array(
            "tanggal_permohonan >="=> $this->input->post('waktu_dari'),
            "tanggal_permohonan <="=> $this->input->post('waktu_sampai'),
        );
        if($this->input->post('id_status')!='-1'){
            $params_where['id_status']= $this->input->post('id_status');
        }
        if($this->input->post('id_tempat_vaksin')!='-1'){
            $params_where['id_tempat_vaksin']= $this->input->post('id_tempat_vaksin');
        }

        $list = $this->Permohonan_vaksin_model->get_datatables($params_where,'permohonan_vaksin.*',"tanggal_permohonan");
        //----------------------
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->tempat_vaksin_nama;
            $row[] = $field->nik;
            $row[] = date("d-m-Y",strtotime($field->tanggal_permohonan));
            $row[] = $field->status_vaksin_nama;
            $row[] = $field->nama;
            $row[] = $field->no_hp;
            $row[] = $field->alamat;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Permohonan_vaksin_model->count_all($params_where),
            "recordsFiltered" => $this->Permohonan_vaksin_model->count_filtered($params_where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}

