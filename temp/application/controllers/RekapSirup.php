<?php

class RekapSirup extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session'); 
        if(!$this->session->has_userdata('nama_adminn')){
            redirect('home/login');   
        }
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
    }

    function debug_to_console($data) {
        $output = $data;
        if (is_array($output))
            $output = implode(',', $output);

        echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
    }

    function index()
    {
       $data['_view'] = 'sirup/rekap';
        $this->load->view('layouts/main',$data);
        //print_r($this->merinci_paket_penyedia('2019','69799'));
        //print_r($this->merinci_pagu_penyedia('2019','69799'));
        
    }

    function generate(){
        $this->createExcell();
        $data['isAvailibleDownload']=1;
        $data['_view'] = 'sirup/rekap';
        $this->load->view('layouts/main',$data);
    }

    function hitung()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tahun','Tahun','required|integer');
        $this->form_validation->set_rules('id_satker','Satker','required|max_length[255]');
        
        if($this->form_validation->run()) {
            $tahun = $this->input->post('tahun');
            $id_satker = $this->input->post('id_satker');
            //$data['total_pagu_penyedia']=$this->hitung_pagu_penyedia($tahun,$id_satker);
            $data['total_pagu_penyedia']=$this->merinci_pagu_penyedia($tahun,$id_satker);
            $data['total_pagu_swakelola']=$this->hitung_pagu_swakelola($tahun,$id_satker);
            $data['total_pagu_penyedia_dalamswakelola']=$this->hitung_pagu_penyedia_dalamswakelola($tahun,$id_satker);
            //$data['total_paket_penyedia']=$this->hitung_paket_penyedia($tahun,$id_satker);
            $data['total_paket_penyedia']=$this->merinci_paket_penyedia($tahun,$id_satker);
            $data['total_paket_swakelola']=$this->hitung_paket_swakelola($tahun,$id_satker);
            $data['total_paket_penyedia_dalamswakelola']=$this->hitung_paket_penyedia_dalamswakelola($tahun,$id_satker);
        }

        $data['_view'] = 'sirup/rekap';
        $this->load->view('layouts/main',$data);
    }

     // *****-----------------API------------------------------------------------
    function hitung_pagu_penyedia($tahun,$idSatker){
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediasatker?tahun='.$tahun.'&idSatker='.$idSatker;
        $data=$this->download_json($url);
        $hitung = 0;
        foreach ($data as $d) {
            $hitung = $hitung + (int)$d[2];
        }
        //$this->debug_to_console("Download pagu penyedia satker = " . $idSatker . " SUKSES");
        return $hitung;
    }
    function hitung_pagu_swakelola($tahun,$idSatker){
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/datarupswakelolasatker?tahun='.$tahun.'&idSatker='.$idSatker;
        $data=$this->download_json($url);
        $hitung = 0;
        foreach ($data as $d) {
            $hitung = $hitung + (int)$d[3];
        }
        //$this->debug_to_console("Download pagu swakelola satker = " . $idSatker . " SUKSES");
        return $hitung;
    }
    function hitung_pagu_penyedia_dalamswakelola($tahun,$idSatker){
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediaswakelolaallrekap?tahun='.$tahun.'&idSatker='.$idSatker;
        $data=$this->download_json($url);
        $hitung = 0;
        foreach ($data as $d) {
            $hitung = $hitung + (int)$d[2];
        }
        //$this->debug_to_console("Download pagu swakelola satker = " . $idSatker . " SUKSES");
        return $hitung;
    }
    function merinci_pagu_penyedia($tahun,$idSatker){
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediasatker?tahun='. $tahun.'&idSatker='.$idSatker;
        $data=$this->download_json($url);
        $pakettender = 0;
        $paketnontender = 0;
        $paketepurchas = 0;
        $paketlain = 0;
        $arraytender=array("Tender","Tender Cepat");
        $arraynontender=array("Pengadaan Langsung", "Penunjukan Langsung");
        $arrayepurch=array("E-Purchasing");
        $arrayother_name=array();
        $arrayjumlah=array();


        foreach ($data as $d) {
            if (in_array($d[3], $arraytender)){
                $pakettender = $pakettender+$d[2];
            }else if (in_array($d[3], $arraynontender)){
                $paketnontender = $paketnontender +$d[2];
            }else if (in_array($d[3], $arrayepurch)){
                $paketepurchas = $paketepurchas +$d[2];
            }else{
                //echo $d[3];
                $paketlain = $paketlain +$d[2];
                if (in_array($d[3], $arrayother_name)){
                    $arrayjumlah[$d[3]] = $arrayjumlah[$d[3]] + $d[2];
                }else{
                    array_push($arrayother_name,$d[3]);
                    $arrayjumlah[$d[3]] = 0;
                    $arrayjumlah[$d[3]] = $arrayjumlah[$d[3]] + $d[2] ;
                }
            }
        }
        
        //print_r($arrayjumlah);
        $tring_other="";
        foreach ($arrayjumlah as $key => $value) {
            $tring_other = $tring_other . $key . " : " . number_format($value) . "" . "\r\n";
        }

        $result= 
            array(  
                'tender' => $pakettender,
                'nontender' => $paketnontender,
                'epurchasing' => $paketepurchas,
                "lain" => $tring_other,
                "total_lain" => $paketlain
            );
        return $result;
    }

    function hitung_paket_penyedia($tahun,$idSatker){
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/datatableruprekapkldi?idKldi=D168&tahun='. $tahun;   
        $data=$this->download_json($url);
        $hitung = 0;
        foreach ($data as $d) {
            if($d[0] == $idSatker){
                $hitung = $d[2];
            }
        }
        //$this->debug_to_console("Download paket penyedia satker = " . $idSatker . " SUKSES");
        return $hitung;
    }
    function hitung_paket_swakelola($tahun,$idSatker){
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/datatableruprekapkldi?idKldi=D168&tahun='. $tahun;   
        $data=$this->download_json($url);
        $hitung = 0;
        foreach ($data as $d) {
            if($d[0] == $idSatker){
                $hitung = $d[4];
            }
        }
        //$this->debug_to_console("Download paket swakelola satker = " . $idSatker . " SUKSES");
        return $hitung;
    }
    function hitung_paket_penyedia_dalamswakelola($tahun,$idSatker){
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/datatableruprekapkldi?idKldi=D168&tahun='. $tahun;   
        $data=$this->download_json($url);
        $hitung = 0;
        foreach ($data as $d) {
            if($d[0] == $idSatker){
                $hitung = $d[6];
            }
        }
        //$this->debug_to_console("Download paket penyedia satker = " . $idSatker . " SUKSES");
        return $hitung;
    }
    function merinci_paket_penyedia($tahun,$idSatker){
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediasatker?tahun='. $tahun.'&idSatker='.$idSatker;
        $data=$this->download_json($url);
        $pakettender = 0;
        $paketnontender = 0;
        $paketepurchas = 0;
        $paketlain =0;
        $arraytender=array("Tender","Tender Cepat");
        $arraynontender=array("Pengadaan Langsung", "Penunjukan Langsung");
        $arrayepurch=array("E-Purchasing");
        $arrayother_name=array();
        $arrayjumlah=array();


        foreach ($data as $d) {
            if (in_array($d[3], $arraytender)){
                $pakettender = $pakettender+1;
            }else if (in_array($d[3], $arraynontender)){
                $paketnontender = $paketnontender +1;
            }else if (in_array($d[3], $arrayepurch)){
                $paketepurchas = $paketepurchas +1;
            }else{
                //echo $d[3];
                $paketlain=$paketlain+1;
                if (in_array($d[3], $arrayother_name)){
                    $arrayjumlah[$d[3]] = $arrayjumlah[$d[3]] + 1;
                }else{
                    array_push($arrayother_name,$d[3]);
                    $arrayjumlah[$d[3]] = 0;
                    $arrayjumlah[$d[3]] = $arrayjumlah[$d[3]] + 1 ;
                }
            }
        }
        
        //print_r($arrayjumlah);
        $tring_other="";
        foreach ($arrayjumlah as $key => $value) {
            $tring_other = $tring_other . $key . " : " . strval($value) . " paket" . "\r\n";
        }

        $result= 
            array(  
                'tender' => $pakettender,
                'nontender' => $paketnontender,
                'epurchasing' => $paketepurchas,
                "lain" => $tring_other,
                "total_lain" => $paketlain
            );
        return $result;
    }
    


    // *****-----------------END Of API---------------------------
    function download_json($url){
         $obj = json_decode($this->curl_get_contents($url), true);
        return $obj['aaData'];
    }
    function curl_get_contents($url){
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      $data = curl_exec($ch);
      curl_close($ch);
      return $data;
    }
    
    
    function createExcell(){
        $this->debug_to_console("PROGREESS" . "0%");
        //$inputFileName = './assets/template.xlsx';
        copy("./assets/template.xlsx","./assets/hasil.xlsx");
        $inputFileName = './assets/hasil.xlsx';
        $inputFileName2 = base_url().'assets/template';
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $this->debug_to_console("PROGREESS" . "30%");
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = 71;
        $highestColumn = "T";
        $tahun = "2020";
        //$highestColumn = $sheet->getHighestColumn();

        //SET JUDUL BOZZ
        $bulan = array (1 =>   
                'JANUARI',
                'FEBRUARI',
                'MARET',
                'APRIL',
                'MEI',
                'JUNI',
                'JULI',
                'AGUSTUS',
                'SEPTEMBER',
                'OKTOBER',
                'NOVEMBER',
                'DESEMBER'
            );
        date_default_timezone_set("Asia/Bangkok");
        $title="REKAP OPD BULAN ".$bulan[(int)date('m')]." RENCANA UMUM PENGADAAN TAHUN ". date('Y');
        $objPHPExcel->getActiveSheet()->setCellValue('B' . 3, $title);

        for ($row = 15; $row <= $highestRow; $row++){ 
            $this->debug_to_console("Mengerjakan Row " . $row);
                //Sesuaikan sama nama kolom tabel di database                               
            $rowData = $sheet->rangeToArray('A' . $row . ':' .  "A". $row,
                NULL,
                TRUE,
                FALSE);
            foreach ($rowData[0] as $value) {
                if($value!=NULL){
                    //echo $row . " - ".$this->hitung_pagu_penyedia($tahun,$value);

                    $array_paket_penyedia=$this->merinci_paket_penyedia($tahun,$value);
                    $array_pagu_penyedia=$this->merinci_pagu_penyedia($tahun,$value);
                    //$pagu_penyedia = $this->hitung_pagu_penyedia($tahun,$value);
                    $pagu_swakelola = $this->hitung_pagu_swakelola($tahun,$value);
                    $pagu_penyedia_dalamswakelola = $this->hitung_pagu_penyedia_dalamswakelola($tahun,$value);
                    $paket_swakelola = $this->hitung_paket_swakelola($tahun,$value);
                    $paket_penyedia_dalamswakelola = $this->hitung_paket_penyedia_dalamswakelola($tahun,$value);
                    // Assign cell values
                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, "");
                    $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $array_paket_penyedia['tender']);
                    $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $array_paket_penyedia['nontender']);
                    $objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $array_paket_penyedia['epurchasing']);
                    $objPHPExcel->getActiveSheet()->setCellValue('N' . $row, $array_paket_penyedia['lain']);
                    $objPHPExcel->getActiveSheet()->setCellValue('O' . $row, $paket_swakelola);
                    $objPHPExcel->getActiveSheet()->setCellValue('P' . $row, $paket_penyedia_dalamswakelola);
                    $objPHPExcel->getActiveSheet()->setCellValue('Q' . $row, (int)$array_paket_penyedia['tender'] + (int)$array_paket_penyedia['nontender'] + (int)$array_paket_penyedia['epurchasing']+ (int)$array_paket_penyedia['total_lain'] + (int)$paket_swakelola + (int)$paket_penyedia_dalamswakelola );
                    

                    $objPHPExcel->getActiveSheet()->setCellValue('R' . $row, $array_pagu_penyedia['tender']);
                    $objPHPExcel->getActiveSheet()->setCellValue('S' . $row, $array_pagu_penyedia['nontender']);
                    $objPHPExcel->getActiveSheet()->setCellValue('T' . $row, $array_pagu_penyedia['epurchasing']);
                    $objPHPExcel->getActiveSheet()->setCellValue('U' . $row, $array_pagu_penyedia['lain']);
                    $objPHPExcel->getActiveSheet()->setCellValue('V' . $row, $pagu_swakelola);
                    $objPHPExcel->getActiveSheet()->setCellValue('W' . $row, $pagu_penyedia_dalamswakelola);
                    $objPHPExcel->getActiveSheet()->setCellValue('X' . $row, (int)$array_pagu_penyedia['tender'] + (int)$array_pagu_penyedia['nontender'] + (int)$array_pagu_penyedia['epurchasing'] + (int)$array_pagu_penyedia['total_lain'] + (int)$pagu_swakelola  + $pagu_penyedia_dalamswakelola);
                    //echo "<br>";
                }
            }

        }       
        // Save it as an excel 2003 file
        $objWriter = IOFactory::createWriter($objPHPExcel, $inputFileType);
        $objWriter->save($inputFileName);

        $this->debug_to_console("PROGREESS" . "100%");
    
    }

    function excell_judul(){
        

        $inputFileName = './assets/hasil.xlsx';
        $inputFileName2 = base_url().'assets/template';
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        $row=3;
        $objPHPExcel->setActiveSheetIndex(0);
        

        // Save it as an excel 2003 file
        $objWriter = IOFactory::createWriter($objPHPExcel, $inputFileType);
        $objWriter->save($inputFileName);


    }
}

