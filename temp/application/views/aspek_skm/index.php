<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Aspek Skm</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('aspek_skm/add'); ?>" class="btn btn-success btn-sm">Tambah</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Nama</th>
						<th>Aksi</th>
                    </tr>
                    <?php foreach($aspek_skm as $a){ ?>
                    <tr>
						<td><?php echo $a['nama']; ?></td>
						<td>
                            <a href="<?php echo site_url('aspek_skm/edit/'.$a['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Ubah</a> 
                            <a href="<?php echo site_url('aspek_skm/remove/'.$a['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Hapus</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
