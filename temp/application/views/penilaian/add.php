<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Tambah Penilaian</h3>
            </div>
            <?php echo form_open('penilaian/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="id_skm" class="control-label">Skm</label>
						<div class="form-group">
							<select name="id_skm" class="form-control">
								<option value="">select skm</option>
								<?php 
								foreach($all_skm as $skm)
								{
									$selected = ($skm['id'] == $this->input->post('id_skm')) ? ' selected="selected"' : "";

									echo '<option value="'.$skm['id'].'" '.$selected.'>'.$skm['id'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_aspek_skm" class="control-label">Aspek Skm</label>
						<div class="form-group">
							<select name="id_aspek_skm" class="form-control">
								<option value="">select aspek_skm</option>
								<?php 
								foreach($all_aspek_skm as $aspek_skm)
								{
									$selected = ($aspek_skm['id'] == $this->input->post('id_aspek_skm')) ? ' selected="selected"' : "";

									echo '<option value="'.$aspek_skm['id'].'" '.$selected.'>'.$aspek_skm['id'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="nilai" class="control-label">Nilai</label>
						<div class="form-group">
							<input type="text" name="nilai" value="<?php echo $this->input->post('nilai'); ?>" class="form-control" id="nilai" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Simpan
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>