<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Tambah Skm</h3>
            </div>
            <?php echo form_open('skm/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="saran" class="control-label">Saran</label>
						<div class="form-group">
							<input type="text" name="saran" value="<?php echo $this->input->post('saran'); ?>" class="form-control" id="saran" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="tanggal" class="control-label">Tanggal</label>
						<div class="form-group">
							<input type="text" name="tanggal" value="<?php echo $this->input->post('tanggal'); ?>" class="has-datetimepicker form-control" id="tanggal" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Simpan
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>