-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Des 2019 pada 08.03
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bukutamu`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `aspek_skm`
--

CREATE TABLE `aspek_skm` (
  `id` int(11) NOT NULL,
  `nama` varchar(259) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aspek_skm`
--

INSERT INTO `aspek_skm` (`id`, `nama`) VALUES
(4, 'Kenyamanan Tempat'),
(5, 'Kelengkapan Fasilitas'),
(6, 'Kecepatan Pelayanan Petugas'),
(7, 'Keramahan Pelayanan Petugas'),
(8, 'Ketepatan Informasi yang Diberikan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kunjungan`
--

CREATE TABLE `kunjungan` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `nama` varchar(999) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `keterangan` varchar(999) NOT NULL,
  `instansi` varchar(999) NOT NULL,
  `id_tujuan` int(11) NOT NULL,
  `email` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kunjungan`
--

INSERT INTO `kunjungan` (`id`, `tanggal`, `nama`, `no_hp`, `keterangan`, `instansi`, `id_tujuan`, `email`) VALUES
(1, '2019-12-03 13:48:30', 'Suep', '0813456432113', 'Daftar', 'CV Mulia Jaya', 4, ''),
(2, '2019-12-03 13:50:24', 'Adi', '0857456789098', 'Verifikasi', 'PT Trunaindo', 4, 'trunaido@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL,
  `nama` varchar(999) NOT NULL,
  `username` varchar(999) NOT NULL,
  `password` varchar(999) NOT NULL,
  `deskripsi` varchar(999) NOT NULL,
  `aktif` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`id`, `nama`, `username`, `password`, `deskripsi`, `aktif`) VALUES
(1, 'Administrator', 'adm', '$2y$12$FZHKTKaQq82BrwRdO2usf.iNK6xml.Ok1E4nU9XjqK1pzRoy2M0CC', 'Administrator Buku Tamu', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penilaian`
--

CREATE TABLE `penilaian` (
  `nilai` int(11) DEFAULT NULL,
  `id_skm` int(11) DEFAULT NULL,
  `id_aspek_skm` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penilaian`
--

INSERT INTO `penilaian` (`nilai`, `id_skm`, `id_aspek_skm`) VALUES
(4, 11, 8),
(3, 11, 7),
(3, 11, 6),
(3, 11, 5),
(4, 11, 4),
(4, 12, 8),
(4, 12, 7),
(3, 12, 6),
(2, 12, 5),
(1, 12, 4),
(5, 13, 8),
(2, 13, 7),
(1, 13, 6),
(3, 13, 5),
(2, 13, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `skm`
--

CREATE TABLE `skm` (
  `id` int(11) NOT NULL,
  `saran` varchar(999) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `skm`
--

INSERT INTO `skm` (`id`, `saran`, `tanggal`) VALUES
(11, 'Petugasnya Ramah', '2019-12-03 13:56:50'),
(12, 'Acnya tdak dingin', '2019-12-03 13:57:55'),
(13, 'Bagus Pelayannnya', '2019-12-09 11:17:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tujuan`
--

CREATE TABLE `tujuan` (
  `id` int(11) NOT NULL,
  `nama` varchar(999) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tujuan`
--

INSERT INTO `tujuan` (`id`, `nama`) VALUES
(4, 'LPSE'),
(5, 'Bidang'),
(6, 'Sekretariat');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `aspek_skm`
--
ALTER TABLE `aspek_skm`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kunjungan`
--
ALTER TABLE `kunjungan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tujuan` (`id_tujuan`);

--
-- Indeks untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  ADD KEY `fk_skm` (`id_skm`),
  ADD KEY `fk_aspek_skm` (`id_aspek_skm`);

--
-- Indeks untuk tabel `skm`
--
ALTER TABLE `skm`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tujuan`
--
ALTER TABLE `tujuan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `aspek_skm`
--
ALTER TABLE `aspek_skm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `kunjungan`
--
ALTER TABLE `kunjungan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `skm`
--
ALTER TABLE `skm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tujuan`
--
ALTER TABLE `tujuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kunjungan`
--
ALTER TABLE `kunjungan`
  ADD CONSTRAINT `kunjungan_ibfk_1` FOREIGN KEY (`id_tujuan`) REFERENCES `tujuan` (`id`);

--
-- Ketidakleluasaan untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  ADD CONSTRAINT `fk_aspek_skm` FOREIGN KEY (`id_aspek_skm`) REFERENCES `aspek_skm` (`id`),
  ADD CONSTRAINT `fk_skm` FOREIGN KEY (`id_skm`) REFERENCES `skm` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
