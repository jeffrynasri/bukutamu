$(document).ready(function() {
  //////////numeric input
  $('.nomor-saja').on("keypress", function (evt) {
    //45-
    //46.
    if (evt.which < 48 || evt.which > 57) {
      //if (evt.which != 8 && evt.which != 0 && evt.which != 45 && evt.which != 46) {
      if (evt.which != 8 && evt.which != 0 && evt.which != 46) {
        evt.preventDefault();
      }
    }
  });
  $('.nomor-saja').focus(function() {
    if (parseFloat(this.value) == "0") {
      this.value = "";
    }
  });
  $('.nomor-saja').focusout(function() {
    if (this.value.trim() == "") {
      this.value = "0";
    }
  });
  //////////numeric input


  //select2
  $('.select2').select2({
    // dropdownParent: $('#div_modal')
    // closeOnSelect: true
    minimumResultsForSearch: -1
  });

  //datepicker
  $('.dtp-hari').datepicker({
    autoclose: true,
    //format: "dd MM yyyy",
    format: "dd-M-yyyy",

    todayHighlight: true,
    //orientation: "top auto",
    // language: "id",
    orientation: "auto",
    todayBtn: true,
    todayHighlight: true,
    startView: "days",
    minViewMode: "days"
  });
  $('.dtp-hari').datepicker('update', new Date());

  //datepicker range
  // $('.dtp-hari-range').daterangepicker();
  //$('.dtp-hari').datepicker('update', new Date());

  $('.dtp-bulan').datepicker({
    autoclose: true,
    //format: "dd MM yyyy",
    format: "MM yyyy",
    todayHighlight: true,
    //orientation: "top auto",
    // language: "id",
    orientation: "auto",
    todayBtn: true,
    todayHighlight: true,
    startView: "months",
    minViewMode: "months"
  });
  $('.dtp-bulan').datepicker('update', new Date());

  $('.dtp-tahun').datepicker({
    autoclose: true,
    format: "yyyy",
    todayHighlight: true,
    //orientation: "top auto",
    // language: "id",
    orientation: "bottom",
    todayBtn: true,
    todayHighlight: true,
    startView: "years",
    minViewMode: "years"
  });
  $('.dtp-tahun').datepicker('update', new Date());


  //hide overlay
  $('.div-loading-overlay').hide();
});

function adjustDT()
{
  setTimeout(function() {
      $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
  }, 350);
}

//read gambar
// function getFileThumbnail(input)
// {
//   if (input != '') {
//     var reader = new FileReader();
//     reader.onload = function (e) {
//       $('#img_tes').attr('src'. e.target.result).width(200);
//     };
//
//     reader.readAsDataURL('http://localhost/esptpd-adm/assets/img/user4-128x128.jpg');
//   }
// }
